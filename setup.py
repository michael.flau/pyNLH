#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  5 11:28:00 2018
@author: Michael Flau
setup file for pynlh module
"""

from setuptools import setup

setup(name='pynlh',
      version='0.1',
      description='pyNLH - Neat Little Helpers. A collection of small helper function useful to me in various projects.',
      url='http://gitlab.com/pyNLH',
      author='Michael Flau',
      author_email='michael@flau.net',
      license='MIT',
      packages=['pynlh'],
      zip_safe=False)