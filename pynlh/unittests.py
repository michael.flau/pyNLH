#!/usr/bin/python3

from pynlh import db_helper
from os import remove
import abc
import sqlite3
import random
from collections import namedtuple
import string
import copy

"""
Example adapter implemenatation which can be registered to
input custom type data.
"""
class probe1(db_helper.AbstractDBInputAdapter):

    __metaclass__ = abc.ABCMeta

    def __init__(self, column):
        super().__init__(column=column)

    def __conform__(self, protocol):

        if not self.hasValue:
            print("No value set, call setDBValue first!")
            return None

        if protocol is sqlite3.PrepareProtocol:
            return "{},{}".format(self.value[0], self.value[1])

    def setDBvalue(self, value):
        assert type(v) is tuple
        assert type(v[0]) is int
        assert type(v[1]) is int

        self.value = value

class converter1(db_helper.AbstractDBOutputAdapter):

    __metaclass__ = abc.ABCMeta

    def __init__(self, column, datatype):
        super().__init__(column=column, datatype=datatype)

    def __convert__(self, sql_value):
        t = sql_value.split(',')
        self.value = (int(t[0]),int(t[1]))

    def getDBvalue(self):
        return self.value

def getDBContent():
    print("Generating DB content...")
    numRows = 1000
    strlen  = 10
    seed    = 42

    random.seed(seed)

    content = []
    rowidx  = 0
    for idx in range(numRows):
        tmp = namedtuple('row',['foo', 'bar', 'baz', 'bazfoo'])
        tmp.foo = rowidx
        tmp.bar = random.random()
        tmp.baz = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(strlen))
        tmp.bazfoo = (random.randint(-1*numRows, numRows), random.randint(-1*numRows, numRows))
        content.append(tmp)
        rowidx += 1

    print("...done")
    return content



class Test_db_helper(object):

    def test_generator(self):

        filename  = "./test.db"
        tablename = "testtable"
        defs      = ["foo|INTEGER", "bar|REAL", "baz|TEXT", "bazfoo|TEXT"]

        assert filename.__class__ is str
        assert tablename.__class__ is str
        assert defs.__class__ is list

        print("UNITTEST PART1 -- CREATION OF DATABASE")

        # Create a generator
        dbgen = db_helper.DBGenerator(filename)
        # create a field set list
        fset = dbgen.createFieldSets(defs)

        # Some locals
        test_false = "fail_foo"

        # Try to create a new table schema with parsed column set
        dbgen.createTable(tablename, fset)

        # Check amount of columns
        assert dbgen.getColumnCount(tablename) == len(defs)
        # Check for existence of specific tables
        assert dbgen.hasTable(tablename) == True
        assert dbgen.hasTable(test_false) == False

        # Check for existence of specific table columns
        for d in defs:
            assert type(d) is str
            assert dbgen.hasTableColumn(tablename, d.split("|")[0]) == True

        assert dbgen.hasTableColumn(tablename, test_false) == False

        # Check for datatype of specified columns
        for d in defs:
            assert type(d) is str
            assert dbgen.getColumnDatatype(tablename, d.split("|")[0]) == d.split("|")[1]

        assert dbgen.getColumnDatatype(tablename, test_false) == False

    def test_writer(self):
        filename  = "./test.db"
        tablename = "testtable"
        defs      = ["foo|INTEGER", "bar|REAL", "baz|TEXT", "bazfoo|TEXT"]

        assert type(filename) is str
        assert type(tablename) is str
        assert type(defs) is list

        print("UNITTEST PART2 -- WRITING TO DATABASE")

        # Create a database writer object
        writer = db_helper.DBWriter(filename, tablename)

        print("Writer has tables: {}".format(writer.table))
        print("Writer's table {} has columns: {}".format(writer.table, writer.columns))

        # Register an alternative adapter for column 'bazfoo'
        alt_adapter = probe1(column="bazfoo")
        writer.registerAlternativeInputAdapter(adapter=alt_adapter)

        # Get last primary key index
        startKey     = writer.currentIndex if writer.currentIndex == 0 else writer.currentIndex + 1
        dbContent    = getDBContent()
        lenDBContent = len(dbContent)


        start = startKey
        end   = startKey + int(lenDBContent/2)

        # Use single value fill method to input values
        for row_idx in range(start,end):
            c_idx = row_idx-startKey
            writer.setSingleColumnValue(data=("foo", dbContent[c_idx].foo))
            writer.setSingleColumnValue(data=("bar", dbContent[c_idx].bar))
            writer.setSingleColumnValue(data=("baz", dbContent[c_idx].baz))
            writer.setSingleColumnValue(data=("bazfoo", dbContent[c_idx].bazfoo))
            writer.execute()

        start = end
        end   = lenDBContent

        # Use mutli value fill method to input values
        for row_idx in range(startKey+int(lenDBContent/2), startKey+lenDBContent):
            c_idx = row_idx - startKey
            d = { "foo": dbContent[c_idx].foo,
                  "bar": dbContent[c_idx].bar,
                  "baz": dbContent[c_idx].baz,
                  "bazfoo": dbContent[c_idx].bazfoo }
            writer.setColumnValues(data=d)
            writer.execute()


    def test_reader(self):

        filename  = "./test.db"
        tablename = "testtable"
        defs      = ["foo|INTEGER", "bar|REAL", "baz|TEXT", "bazfoo|TEXT"]

        assert type(filename) is str
        assert type(tablename) is str
        assert type(defs) is list

        print("UNITTEST PART3 -- READING FROM DATABASE")
        # Create a Reader instance
        reader = db_helper.DBReader(database=filename, table=tablename)

        # test if table is registered
        assert reader.table == tablename
        # Test if columns are registered
        for c in reader.columns:
            hasEntry = False
            for c_def in defs:
                if c == c_def.split('|')[0]:
                    hasEntry = True
            assert hasEntry

        # register an alternative converter for column 'bazfoo'
        alt_converter = converter1(column='bazfoo', datatype=tuple)
        reader.registerAlternativeOutputConverter(converter=alt_converter)

        dbContent    = getDBContent()
        lenDBContent = len(dbContent)
        maxKey       = reader.currentIndex

        assert lenDBContent == maxKey+1

        for i in range(maxKey):
            reader.requestTableRowValues(i)
            values = reader.retrieve()
            # print(values)
            assert type(values["foo"]) is int
            assert type(values["bar"]) is float
            assert type(values["baz"]) is str
            assert type(values["bazfoo"]) is tuple

            assert dbContent[i].foo == values["foo"]
            assert dbContent[i].bar == values["bar"]
            assert dbContent[i].baz == values["baz"]
            assert dbContent[i].bazfoo == values["bazfoo"]
            # print("|".join(["{}".format(values[key]) for key in values.keys()]))

    def test_updater(self):
        filename  = "./test.db"
        tablename = "testtable"
        defs      = ["foo|INTEGER", "bar|REAL", "baz|TEXT", "bazfoo|TEXT"]

        assert type(filename) is str
        assert type(tablename) is str
        assert type(defs) is list

        print("UNITTEST PART4 -- UPDATING DATABASE ENTRIES")

        reader = db_helper.DBReader(database=filename, table=tablename)
        alt_converter = converter1(column='bazfoo', datatype=tuple)
        reader.registerAlternativeOutputConverter(converter=alt_converter)

        # Create a database writer object
        writer = db_helper.DBWriter(filename, tablename)
        alt_adapter = probe1(column="bazfoo")
        writer.registerAlternativeInputAdapter(adapter=alt_adapter)

        maxkey   = reader.getCurrentPrimaryKeyIndex(tablename)
        alterkey = int(maxkey/2)
        reader.requestTableRowValues( alterkey )
        values = reader.retrieve()

        new_value = "THIS COLUMN WAS JUST ALTERED"
        old_value = values["baz"]
        values["baz"] = new_value

        writer.setColumnValues(values)
        writer.update( alterkey )

        reader.requestTableRowValues(alterkey)
        values2 = reader.retrieve()

        assert values2["baz"] == new_value
        assert values2["baz"] != old_value
        assert values["foo"] == values2["foo"]
        assert values["bar"] == values2["bar"]
        assert values["bazfoo"] == values2["bazfoo"]

        remove("./test.db")
