#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json

"""
"""
class ConfigReader(object):
    def __init__(self, cfgfile_path):
        try:
            self.m_cfg = None
            with open(cfgfile_path, 'r') as fp:
                self.m_cfg = json.load(fp)

            self.m_initialized = True
        except Exception as e:
            print("Failed to load config file from passed string: {}\n\t>> Error: {}".format(cfgfile_path, e))
            self.m_initialized = False

    """
    """
    def getFieldValue(self, fieldname):
        assert self.m_initialized
        assert self.hasfield(fieldname)
        return self.m_cfg[fieldname]

    """
    """
    def findKey(self, key, iterable=None):

        if iterable == None:
            iterable = self.m_cfg

        assert type(iterable) is dict
        assert type(key) is str
        ret = None
        for k, v in iterable.items():
            if k == key:
                # print("Found key {}, matches {}".format(k, key))
                ret = v
            else:
                if type(v) is dict:
                    # print("Value of key {} is dict".format(k))
                    ret = self.findKey(key, v)

            if ret != None:
                # print("Type of ret {} is not None, break".format(ret))
                break

        return ret



    """
    """
    def hasfield(self, key):
        assert self.m_initialized
        assert isinstance(key, str)
        return key in self.m_cfg

    """
    """
    def traverseConfig(self, iterable, depth=0,):
        assert type(iterable) is dict
        for k, v in iterable.items():
            if type(v) is dict:
                print(('\t' * depth) + '|-' + k)
                print(('\t' * depth) + ('-'*8) + '+')
                self.traverseConfig(v, depth+1)
            else:
                print(('\t' * depth)+ '|-' + k + ' >> ' + str(v))

    """
    """
    @property
    def keytree(self):
        self.traverseConfig(self.m_cfg)

    @property
    def root(self):
        return self.m_cfg