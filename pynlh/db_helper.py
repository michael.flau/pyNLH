# -*- coding: utf-8 -*-

"""
Copyright 2018, Michael Flau <michael@flau.net>
"""

import sqlite3
import abc
from collections import namedtuple
from pathlib import Path

from .db_adapter import *

class AbstractDBAccess(object):
    """
    Abstract Sqlite3 base class, all creatable classes
    refer to this class as parent.
    """
    def __init__(self, db_filename):
        """
        Ctor, opens the database. Tries to create a new database. If database already present
        it is simply opened.
        :param db_filename: database file location as str object
        """
        self._m_open  = False
        self.m_db     = None
        self._m_index = 0

        assert type(db_filename) is str

        try:
            tmpdir = db_filename
            isdir  = Path(tmpdir)
            if not isdir.is_absolute():
                tmpdir = isdir.absolute().as_posix()
        except sqlite3.Error as e:
            print("Error{} \n\t>> Failed to make db path {} absolute".format(e, db_filename))
            raise e

        self.db_filename = tmpdir
        # Open database
        self.open()

    def __del__(self):
        """
        Dtor, commits changes, closes database file.
        :return: None
        """
        self.close()

    def open(self):
        """
        Opens database, automatically called from ctor.
        :return: None
        """
        if not self.isopen:
            try:
                self.m_db = sqlite3.connect(self.db_filename, detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES)
            except sqlite3.Error as e:
                print("{} ERROR --\n\t>> Failed to open data base file at: {}".format(self.__class__.__name__, e))
                self.isopen = False
                raise e
            self.isopen = True
            print("Database object successfully opened...")

    def sync(self):
        """
        Explicitly triggers sync of database state. Raises sqlite3.OperationError
        if commit can't be executed.
        :return: None
        """
        if not self.isopen:
            print("Can't sync database state, database is not open.")
            return

        try:
            self.m_db.commit()
        except sqlite3.OperationalError as e:
            print("{} ERROR -- Failed to sync database state\n\t>> {}".format(__class__.__name__, e))
            raise e

    def close(self):
        """
        Closes the database, automatically called from dtor. Raises sqlite3.Error if
        commit or close of database fails.
        :return: None
        """

        if not self.isopen:
            print("Database not open, skip destruction")
            return

        try:
            self.sync()
            self.m_db.close()
            self.isopen = False
            print("Database object closed and destroyed...")
        except sqlite3.Error as e:
            print("{}: Failed to close data base file at: {}".format(self.__class__.__name__, e))
            raise e

    def hasTable(self, table):
        """
        Check if a table schema already exists inside of a sqlite database file.
        :param table: tablename as str object.
        :return: True if table exists, False if not
        """
        assert type(table) is str
        assert table != ""

        if not self.isopen:
            print("{} -- WARN: Database not open, can't check for tables".format(self.__class__.__name__))
            return False

        command = "SELECT name FROM sqlite_master WHERE type='table' AND name='{table_name}'".format(table_name=table)
        dbc     = self.m_db.cursor()
        dbc.execute(command)
        v       = dbc.fetchall()

        return False if (len(v) == 0) or (v[0][0] == "") else True

    def getColumnCount(self, table):
        """
        Returns the number of columns of an existing table schema.
        :param table: Name of table schema as str object
        :return: Number of columns as integer
        """
        assert type(table) is str
        assert table != ""

        if not self.isopen:
            print("{} -- WARN: Database not open, can't check for columncount".format(self.__class__.__name__))
            return False

        if not self.hasTable(table):
            print("{} -- WARN: Table schema does not exist, can't check for columncount".format(self.__class__.__name__))
            return False

        command = "PRAGMA table_info({})".format(table)
        dbc     = self.m_db.cursor()
        dbc.execute(command)
        v = dbc.fetchall()
        return len(v)

    def hasTableColumn(self, table, column):
        """
        Check if a table has a column of given name.
        :param table: Name of table schema as str object
        :param column: Name of column as str object
        :return: True if column exists within table schema, False if not
        """
        assert type(table) is str
        assert table != ""

        assert type(column) is str
        assert column != ""

        if not self.isopen:
            print("{} -- WARN: Database not open, can't check for columnnames".format(self.__class__.__name__))
            return False

        if not self.hasTable(table):
            print("{} -- WARN: Table schema does not exist, can't check for columnnames".format(self.__class__.__name__))
            return False

        command = "PRAGMA table_info({})".format(table)
        dbc     = self.m_db.cursor()
        dbc.execute(command)
        v = dbc.fetchall()

        for c in v:
            if c[1] == column:
                return True

        #print("Columns of table {}: {}".format(table,v))
        return False

    def getColumnDatatype(self, table, column):
        """
        Retrieve the datatype of a column,
        by passing the table name and the column name.
        :param table: Name of existing table schema as string
        :param column: Name of contained column as string
        :return: Datatype name as string as defined by NOSQL documentation
        """
        assert type(table) is str
        assert table != ""

        assert type(column) is str
        assert column != ""

        if not self.isopen:
            print("{} -- WARN: Database not open, can't check for columnnames".format(self.__class__.__name__))
            return False

        if not self.hasTable(table):
            print("{} -- WARN: Table schema does not exist, can't check for columnnames".format(self.__class__.__name__))
            return False

        if not self.hasTableColumn(table, column):
            print("{} -- WARN: Table column does not exist, can't check for column datatype".format(self.__class__.__name__))
            return False

        command = "PRAGMA table_info({})".format(table)
        dbc     = self.m_db.cursor()
        dbc.execute(command)
        v = dbc.fetchall()

        for c in v:
            if c[1] == column:
                return c[2]

        #This should never be reached
        return "NONE"

    def getColumnNameByIndex(self, table, index):
        """
        Retrieve the name of a column in a given table by its index.
        :param table: Name of table to be referenced as str
        :param index: Index of column as int
        :return: Column name as str
        """
        assert type(table) is str
        assert table != ""

        assert type(index) is int
        assert index >= 0

        if not self.isopen:
            print("{} -- WARN: Database not open, can't check for columnnames".format(self.__class__.__name__))
            return False

        if not self.hasTable(table):
            print("{} -- WARN: Table schema does not exist, can't check for columnnames".format(self.__class__.__name__))
            return False

        if self.getColumnCount(table=table) <= index:
            print("{} -- WARN: passed index is not within bounds of schema of given table name!".format(self.__class__.__name__))
            return False

        command = "PRAGMA table_info({})".format(table)
        dbc     = self.m_db.cursor()
        dbc.execute(command)
        v       = dbc.fetchall()

        for c in v:
            if c[0] == index:
                return c[1]

    def getColumns(self, table):
        """
        Returns a list of all columns present in table schema.
        :param table: table name as str object
        :return: list of all columns as str objects
        """
        assert type(table) is str
        assert table != ""

        if not self.isopen:
            print("{} -- WARN: Database not open, can't retrieve columnnames".format(self.__class__.__name__))
            return None

        if not self.hasTable(table):
            print("{} -- WARN: Table schema does not exist, can't retrieve columnnames".format(self.__class__.__name__))
            return None

        try:
            command = "PRAGMA table_info({})".format(table)
            dbc     = self.m_db.cursor()
            dbc.execute(command)
            v       = dbc.fetchall()
        except sqlite3.OperationalError as e:
            print("{} ERROR -- Failed to get table info\n\t>>{}".format(self.__class__.__name__, e))
            raise e

        cols = []
        for c in v:
            cols.append(c[1])

        return cols

    def getPrimaryKeyColumn(self, table):
        """
        Returns the name of the column which is stores the primary key.
        :param table: tablename as str object
        :return: column name as str object
        """
        assert type(table) is str
        assert table != ""

        if not self.isopen:
            print("{} -- WARN: Database not open, can't retrieve primary key columnname".format(self.__class__.__name__))
            return None

        if not self.hasTable(table):
            print("{} -- WARN: Table schema does not exist, can't retrieve primary key columnname".format(self.__class__.__name__))
            return None

        try:
            command = "PRAGMA table_info({})".format(table)
            dbc     = self.m_db.cursor()
            dbc.execute(command)
            v       = dbc.fetchall()
        except sqlite3.OperationalError as e:
            print("{} ERROR -- Failed to get table info\n\t>>{}".format(self.__class__.__name__, e))
            raise e

        pk_col = None

        # Iterate over all columns
        for c in v:
            # if primary key field is set to 1, the columns is marked as primary key column
            if c[5] == 1:
                # Store name of column in return value and break
                pk_col = c[1]
                break

        return pk_col

    def getCurrentPrimaryKeyIndex(self, table):
        """
        Return the most current primary index of given table.
        The user needs to specifiy only the table name, the method will
        determine the primary key column by itself.
        :param table: Name of tables contained in open database file
        :return: the stored current primary key index as integer
        """
        assert type(table) is str

        if not self.hasTable(table):
            print("{} -- WARN: Table schema does not exist, can't check for current primary key index".format(self.__class__.__name__))
            return False

        # First find the column which is assigned as primary key
        column = self.getPrimaryKeyColumn(table=table)

        if column is None:
            print("{} ERROR -- No primary key found, can't proceed".format(__class__.__name__))
            raise ValueError

        # Now use found column name to extract max value from column as current key
        command2 = "SELECT MAX({}) AS currentIndex from {}".format(column, table)
        dbc = self.m_db.cursor()
        dbc.execute(command2)
        v = dbc.fetchone()
        # print("Current index: {}".format(v[0]))

        if v[0] is None:
            self._m_index = None
        else:
            self._m_index = int(v[0])

        print("Retrieved current max index as {}".format(self._m_index))
        return self._m_index

    def primaryKeyExists(self, table, key):
        """
        Check if a primary key exists in database
        :param table:
        :param key:
        :return:
        """

        if not self.hasTable(table):
            print("{} -- WARN: Table schema does not exist, can't check for current primary key index".format(self.__class__.__name__))
            return False

        # First find the column which is assigned as primary key
        column = self.getPrimaryKeyColumn(table=table)

        if column is None:
            print("{} ERROR -- No primary key found, can't proceed".format(__class__.__name__))
            raise ValueError

        # Now use found column name to extract max value from column as current key
        command = "SELECT {} FROM {} WHERE {}=={}".format(column, table, column, key)
        dbc = self.m_db.cursor()
        dbc.execute(command)
        v = dbc.fetchone()

        if v is None:
            ret = False
        else:
            ret = True

        return ret

    @property
    def isopen(self):
        """
        Simple getter to check whether a database file is opened.
        :return: True if open, False if not
        """
        return self._m_open

    @isopen.setter
    def isopen(self, state):
        """
        Simple setter to alter the open state of the database object.
        :param state: Open state as boolean value.
        :return: None
        """
        self._m_open = state

    @property
    def databaseFilename(self):
        """
        Simple getter to retrieve the name of the referenced database filename.
        :return: Database file name as str object
        """
        return self.db_filename

    @property
    def currentIndex(self):
        """
        Returns the current maximum primary key as index. This is useful in order to
        know where to insert new rows, since the primary key must fullfill the UNIQUE
        constraint and thus must not occur twice.
        :return: Current maximum primary key as integer
        """
        return self._m_index


class DBGenerator(AbstractDBAccess):
    """
    Generator class whose mere intent is to create new databases
    """
    def __init__(self, db_filename):
        """
        Ctor of database generator. Creates and/or opens a new/existing
        database file.
        :param db_filename: database filename as str object
        """
        try:
            super().__init__(db_filename)
        except sqlite3.Error as e:
            print("{} -- ERROR\n\t>> Failed call to parent constructor in {}".format(self.__class__.__name__, e))

        self._field_template = namedtuple('field', ["id", "datatype"])

    def __del__(self):
        """
        Dtor, calls base class dtor.
        :return:
        """
        super().__del__()

    def createTable(self, table, fields):
        """
        Create a database's tables. Method requires a tablename and a list of
        field descriptors, which need to be named tuples, containing the field
        IDs 'id' and 'datatype'.
        :param table: name ot table which is supposed to be created as str object
        :param fields: list of field descriptors as named tuples, each containing fields 'id' and datatype.
        :return: None
        """
        assert type(table) is str
        assert type(fields) is list
        # Helper closure
        def createFields(f):
            return "{} {} PRIMARY KEY,".format(f[0].id, f[0].datatype) + \
                   ",".join(["{} {}".format(field.id, field.datatype) for field in f[1:]])

        try:
            command = "CREATE TABLE {} ({})".format(table, createFields(fields))
            dbc     = self.m_db.cursor()
            # print("Executing command: {}".format(command))
            dbc.execute( command )
        except sqlite3.OperationalError as e:
            print('{} -- WARN {}\n\t>> Failed to execute table creation with command: {}'.format(self.__class__.__name__,
                                                                                                 e,
                                                                                                 command))

    def createFieldSet(self, definition):
        """
        Create a single field object. Helper method to create a field descriptor object which can be passed
        to the createTable() method.
        :param definition: field definition as str object. The string needs to have the shape "COLUMNNAME|DATATYPE", e.g. "temperature|REAL"
        :return: fieldset definition as named tuple
        """
        assert type(definition) is str
        parts = definition.split('|')
        assert len(parts) > 1
        ret = self._field_template(parts[0], parts[1])
        return ret

    def createFieldSets(self, definitions):
        """
        Creates a list of field sets as defined by input list.
        :param definitions: List of field defintions as str objects following the pattern shape decribed in createFieldSet() method.
        :return: List of named tuples, each describing one column
        """
        assert type(definitions) is list
        fs = []
        for definition in definitions:
            fs.append( self.createFieldSet(definition) )
        return fs


class DBWriter(AbstractDBAccess):
    """
    Writer class which is supposed to insert new values-,
    and update values already contained in the database,
    if the user changes those values.
    """
    def __init__(self, database, table):
        """
        Ctor of DBWriter. Upon creation the writer initialized itself with default values, based on given
        database file name and table name.
        :param database:
        :param table:
        """
        try:
            super().__init__(db_filename=database)

            if not self.hasTable(table=table):
                print("{} ERROR -- Can't create DBWriter object, no such table".format(self.__class__.__name__))
                self._initialized = False
                return

            # Get current primary key as index
            cpk     = self.getCurrentPrimaryKeyIndex(table=table)
            # Get list of column names
            columns = self.getColumns(table=table)
            # print("{} INFO -- Current PK is {}".format(__class__.__name__, cpk))
        except Exception as e:
            print("{} ERROR\n\t>> Call to base class ctor failed: {}".format(self.__class__.__name__,e))
            raise e

        if (table is not None) and (not self.hasTable(table)):
            raise ValueError("{} ERROR\n\t>> Database has no such table registered, us generator first!".format(__class__.__name__))

        self._table       = table
        self._adapters    = []
        self._initialized = False

        # If passed table name, is an existing table,
        # initialize default adapters for all columns
        if self._table is not None:

            for c_idx in range(self.getColumnCount(self._table)):
                sqlite3.register_adapter(columns[c_idx], DefaultDBInputAdapter.__conform__)
                self._adapters.append(DefaultDBInputAdapter( self.getColumnNameByIndex(self._table, c_idx) ))

            assert len(self._adapters) == self.getColumnCount(self._table)
            self._initialized = True
        else:
            self._initialized = False

    def registerAlternativeInputAdapter(self, adapter, dbg=False):
        """
        Exchange the default input adapter of a column for a
        custom adapter defined by the user.
        :param adapter: Custom adapter object, must be derived from
                        AbstractDBInputAdapter.
        :return: None
        """
        assert issubclass(type(adapter), AbstractDBInputAdapter)
        assert self.hasTableColumn(self.table, adapter.targetColumn)

        if dbg:
            print("Register new adapter for column: {}".format(adapter.targetColumn))

        self._adapters = [adapter if adapter.targetColumn == reg_adapter.targetColumn else reg_adapter
                          for reg_adapter in self._adapters]

        sqlite3.register_adapter(adapter.targetColumn, adapter.__conform__)

    def setSingleColumnValue(self, data):
        """
        Set a single value into a specified column adapter.
        :param data: User data which must be  of type tuple. data[0] must be a str object containing
                     the column to insert data into. data[1] must be the data to write itself and must be
                     conform to the registered columns adapter's __conform__ method to enable the adapter
                     to write it to the tabel column.
        :return: None
        """
        assert type(data) is tuple
        assert type(data[0]) is str
        for reg_adapter in self._adapters:
            if reg_adapter.targetColumn == data[0]:
                reg_adapter.value = data[1]

    def setColumnValues(self, data):
        """
        Assign a new set of values to all registered adapters.
        :param data: data must be a dictionary of all data fields planned to write.
                     the key of each dict field is the supposed to be the column name.
                     the key value, is the data itself.
        :return: None
        """
        assert type(data) is dict
        for key in data.keys():
            for adapter in self._adapters:
                if adapter.targetColumn == key:
                    adapter.value = data[key]

    def execute(self, dbg=False):
        """
        Write all set values to database. The values written to the database will
        always be the values last written to the column adapters by using setColumnValues
        setSingleColumnValue method.
        :return: None
        """
        assert self.isopen

        dbc     = self.m_db.cursor()

        command = "INSERT INTO {} ({}) VALUES ({})".format(self._table,
                                                           ",".join([a.targetColumn for a in self._adapters]),
                                                           ",".join(["?" for i in range(self.getColumnCount(self._table))]))
        if dbg:
            print("Executing command: {}".format(command))

        dbc.execute(command, tuple(self._adapters))
        self.sync()

        # for a in self._adapters:
        #     if a.value is not None:
        #         dbc.execute(command, (a,))

        # self.sync()
        self._resetAdapter()


    def update(self, pkidx):
        """
        Updates an existing row, based on the primary key index passed to this method.
        :param pkidx: primary keyindex as integer
        :return: None
        """

        assert self.isopen
        assert (pkidx >= 0) and (pkidx <= self.getCurrentPrimaryKeyIndex(self.table))

        dbc = self.m_db.cursor()


        for a in self._adapters:

            if a.value is None:
                continue

            to_be_updated = ''

            # for a in self._adapters:
            #     if a.value is not None:
            to_be_updated += '{}=?'.format(a.targetColumn)

            # if to_be_updated[-1] == ',':
            #     to_be_updated = to_be_updated[:-1]

            if to_be_updated == '':
                print("No fields to update")
                return

            command = "UPDATE {} SET {} WHERE {} == {}".format(self.table,
                                                               to_be_updated,
                                                               self.getPrimaryKeyColumn(self.table),
                                                               pkidx)

            # print("Executing UPDATE command:\n\t{}".format(command))
            dbc.execute(command, (a,))

        self.sync()
        self._resetAdapter()


    def updateSelectedFields(self):
        pass


    def _resetAdapter(self):
        for a in self._adapters:
            a.value = None


    @property
    def table(self):
        """
        Simple getter to retrieve the tablename, which is reference by the writer object
        :return: tablename as str
        """
        return self._table

    @table.setter
    def table(self, table):
        """
        Simple setter to change the reference table name on the fly
        :param table: Name of table as str object
        :return: None
        """
        assert type(table) is str
        assert self.isopen
        assert self.hasTable(table=table)
        self._table = table

    @property
    def columns(self):
        """
        Returns a list of all columns contained by currently referenced table name.
        :return: list object of all columns names as str objects
        """
        return self.getColumns(self.table)


class DBReader(AbstractDBAccess):
    """
    Data base reader class. Meant for extracting data from the SQL database, either by relying on
    the default converters with which the reader instances are initialized, or by injecting a custom
    converter which needs to be derived from AbstractDBOutputAdapter.
    """

    def __init__(self, database, table):
        """
        Ctor of Reader object, initializes the column converters to DefaultDBOutputAdapter objects on creation.
        Custom Converters need to be registered afterwards.
        :param database: Database filename
        :param table: Referenced table name
        """
        try:
            super().__init__(db_filename=database)

            if not self.hasTable(table=table):
                print("{} ERROR -- Can't create DBReader object, no such table".format(self.__class__.__name__))
                self._initialized = False
                return

            cpk = self.getCurrentPrimaryKeyIndex(table=table)
            # print("{} INFO -- Current PK is {}".format(__class__.__name__, cpk))
            columns = self.getColumns(table)
        except Exception as e:
            print("{} ERROR\n\t>> Call to base class ctor failed: {}".format(self.__class__.__name__,e))
            raise e

        if (table is not None) and (not self.hasTable(table)):
            raise ValueError("{} ERROR\n\t>> Database has no such table registered, us DBGenerator first!".format(__class__.__name__))

        self._table       = table
        self._converters  = []
        self._initialized = False

        if self._table is not None:

            for c_idx in range(self.getColumnCount(self._table)):
                c_name = self.getColumnNameByIndex(self._table, c_idx)
                c_dt = self.getColumnDatatype(table=self.table, column=c_name)

                # print("Register datatype {} for column {}".format(c_dt, c_name))

                self._converters.append(DefaultDBOutputAdapter(column=c_name, datatype=c_dt))
                sqlite3.register_converter(columns[c_idx], self._converters[-1].__convert__)

            assert len(self._converters) == self.getColumnCount(self._table)
            self._initialized = True
        else:
            self._initialized = False

    def registerAlternativeOutputConverter(self, converter):
        """
        Register user defined custom converter function to turn an SQL datatype into
        a python datatype. This can be used to alter the default behaviour of the Reader
        which uses DefaultDBOutputAdapter to retrieve column values.
        :param converter: Converter object, must be derived from AbstractDBOutputAdapter
        :return: None
        """
        assert issubclass(type(converter), AbstractDBOutputAdapter)
        assert self.hasTableColumn(self.table, converter.targetColumn)

        self._converters = [converter if converter.targetColumn == reg_converter.targetColumn else reg_converter
                          for reg_converter in self._converters]

        sqlite3.register_converter(converter.targetColumn, converter.__convert__)

    def requestSingleColumnValue(self, column, index):
        """
        Updates the value stored the converter instance referenced by the passed column name.
        All other values of all other converter instances, will remain unchanged. This method
        does not return anythin, it merely extracts the value from the data base and stores it
        in the referenced converter object's value property. Use retrieve() to actually retrieve
        values from the reader.
        :param column: Column name as str object
        :param index: Index of row as integer
        :return: None
        """
        assert type(column) is str
        assert type(index) is int

        if self.hasTableColumn(table=self.table, column=column):
            print("{} ERROR -- Referenced table has no such column '{}'".format(__class__.__name__,
                                                                                column))
            raise ValueError

        max_idx = self.getCurrentPrimaryKeyIndex(table=self.table)

        if index < 0 or index > max_idx:
            print("{} ERROR -- Requested index, is out of bounds of referenced table column '{}'".format(__class__.__name__,
                                                                                                         column))
            raise ValueError

        try:
            dbc     = self.m_db.cursor()
            command = "SELECT {} from {} WHERE {} == {}".format(column,
                                                                self.table,
                                                                self.getPrimaryKeyColumn(self.table),
                                                                index)
            print("{} -- Executing {}".format(__class__.__name__, command))
            dbc.execute(command)
            new_val = dbc.fetchone[0]
        except sqlite3.OperationalError as e:
            print("{} ERROR -- Failed to request single value of column '{}' at index {}".format(__class__.__name__,
                                                                                                 column,
                                                                                                 index))
            raise e

        assert new_val is not None
        for reg_converter in self._converters:
            if reg_converter.targetColumn == column:
                reg_converter.__convert__(new_val)

    def requestTableRowValues(self, index):
        """
        Updates all values of all stored converter objects by requesting a full row at given index.
        :param index: Index of row as integer
        :return: None
        """
        assert type(index) is int
        assert type(index) is not None

        max_idx = self.getCurrentPrimaryKeyIndex(table=self.table)

        if index < 0 or index > max_idx:
            print("{} ERROR -- Requested index, is out of bounds of referenced table column '{}'".format(__class__.__name__))
            raise ValueError

        try:
            self.m_db.row_factory = sqlite3.Row
            dbc                   = self.m_db.cursor()
            command               = "SELECT * from {} WHERE {} == {}".format(self.table,
                                                                             self.getPrimaryKeyColumn(self.table),
                                                                             index)
            # print("{} -- Executing {}".format(__class__.__name__, command))
            dbc.execute(command)
            row = dbc.fetchone()
            # print(type(row))
            # print(row)
            for key in row.keys():
                for reg_converter in self._converters:
                    if reg_converter.targetColumn == key:
                        reg_converter.__convert__(row[key])
        except sqlite3.OperationalError as e:
            print("{} ERROR -- Failed to request values at index {}".format(__class__.__name__, index))
            raise e

    def requestCustomQueryValues(self, query):
        """
        Attempts to execute the input query which must follow SQL notation.
        :param query: Input query as string
        :return: Query result
        """

        assert type(query) is str
        assert query != ""
        ret = []

        try:
            self.m_db.row_factory = sqlite3.Row
            dbc  = self.m_db.cursor()
            dbc.execute(query)
            rows = dbc.fetchall()

            for row in rows:
                next = {}
                for key in row.keys():
                    for reg_converter in self._converters:
                        if reg_converter.targetColumn == key:
                            # print("trying to convert {} with key {}".format(row[key], key))
                            reg_converter.__convert__(row[key])
                            next[key] = reg_converter.value
                ret.append(next)

                # print("{} -- Class: {}".format(row["uid"], row["classification"]))

        except sqlite3.OperationalError as e:
            print("{} ERROR -- Failed to request values".format(__class__.__name__))
            raise e

        return ret

    def retrieve(self):
        """
        Retrieves all values of all converter functions and returns them as dict
        :return: values as dict object
        """
        ret = {}
        for reg_converter in self._converters:
            ret[reg_converter.targetColumn] = reg_converter.value

        return ret

    @property
    def table(self):
        """
        Simple getter to retrieve the tablename, which is reference by the writer object
        :return: tablename as str
        """
        return self._table

    @table.setter
    def table(self, table):
        """
        Simple setter to change the reference table name on the fly
        :param table: Name of table as str object
        :return: None
        """
        assert type(table) is str
        assert self.isopen
        assert self.hasTable(table=table)
        self._table = table

    @property
    def columns(self):
        """
        Returns a list of all columns contained by currently referenced table name.
        :return: list object of all columns names as str objects
        """
        return self.getColumns(self.table)


# class AbstractDBAdapter(object):
#     """
#     Pure virtual base class of all adapters.
#     """
#     __metaclass__ = abc.ABCMeta
#
#     def __init__(self,column):
#         """
#         Ctor of abstract base class
#         :param column: name of column to be referenced
#         """
#         assert type(column) is str
#         self._m_columnid = column
#         self._m_query    = None
#         self._m_hasQuery = False
#         self._m_value    = None
#         self._m_hasValue = False
#
#     @abc.abstractmethod
#     def __conform__(self, protocol):
#         """
#         Converter function which any adapter must implement
#         :return: SQL or python representation
#         """
#         return
#
#     @property
#     def targetColumn(self):
#         """
#         Returns the registered column name as string.
#         :return: name of the referenced column as string
#         """
#         return self._m_columnid
#
#     @property
#     def hasValue(self):
#         """
#         Simple getter to check if adapter has a value set to write/read
#         :return:  None
#         """
#         return self._m_hasValue
#
#     @property
#     def hasQuery(self):
#         """
#         Simple getter to check if adapter has a value set to write/read
#         :return:
#         """
#         return self._m_hasQuery
#
#     @property
#     def value(self):
#         """
#         Simple getter to retrieve a set value from an adapter
#         :return: value if set, otherwise None
#         """
#         if not self.hasValue:
#             print("{} WARN: No value set in this class instance!".format(__class__.__name__))
#             return None
#
#         return self._m_value
#
#     @property
#     def query(self):
#         """
#         Simple getter to retrieve as set query string from an adapter
#         :return: query as str, if query not set returns None
#         """
#         if not self.hasQuery:
#             print("{} WARN: No query set in this class instance!".format(__class__.__name__))
#             return None
#
#         return self._m_query
#
#     @query.setter
#     def query(self, q):
#         """
#         Setter method for defining a query string for given adapter
#         :param q: SQL query as string
#         :return: None
#         """
#         assert type(q) is str
#         self._m_query = q
#
#
# class AbstractDBInputAdapter(AbstractDBAdapter):
#     """
#     Base class for all custom adapter classes. This base class dictates which
#     methods need to be reimplemented in a adapter leaf class in order to function
#     properly within the DBWriter context.
#     """
#     __metaclass__ = abc.ABCMeta
#
#     def __init__(self, column):
#         """
#         Ctor of input adapter base class, calls ctor of base class.
#         :param column: Name of reference database column.
#         """
#         super().__init__(column)
#
#     @abc.abstractmethod
#     def __conform__(self, protocol):
#         """
#         Converter function which any adapter must implement. This function basically gives the
#         DBWriter instance a recipe for how to convert data from python to SQL.
#         :return: SQL or python representation
#         """
#         return
#
#     @abc.abstractmethod
#     def setDBvalue(self, value):
#         """
#         Abstract formwarding of base class method. Needs to be overwritten by user, in order to
#         define a proper adapter method to write custom data to database.
#         :param value: Custom input values
#         :return: None
#         """
#         return
#
#     @property
#     def value(self):
#         """
#         Simple getter to retrieve a set value from an adapter
#         :return: value if set, otherwise None
#         """
#         if not self.hasValue:
#             print("{} WARN: No value set in this class instance!".format(__class__.__name__))
#             return None
#
#         return self._m_value
#
#     @value.setter
#     def value(self, v):
#         """
#         Simple setter to assign a new value to the adapter's value field for later
#         database insertion.
#         :param v:
#         :return:
#         """
#         assert type(v) is not None
#         self._m_value = v
#         self._m_hasValue = True
#
#
# class AbstractDBOutputAdapter(AbstractDBAdapter):
#     """
#     Base class implementation of custom converter objects.
#     Any converter which is supposed to be registered must be derived
#     from this base class, in order to fulfill the constraints defined here.
#     """
#     __metaclass__ = abc.ABCMeta
#
#     def __init__(self, column, datatype = None):
#         """
#         Ctor of input adapter base class, calls ctor of base class.
#         :param column: Name of reference database column.
#         """
#         super().__init__(column)
#         self._m_datatype = datatype
#         self._m_hasDatatype = False if datatype is None else True
#
#     @abc.abstractmethod
#     def __convert__(self, sql_value):
#         """
#         Converter function which any output adapter must implement
#         :return: SQL to python representation
#         """
#         return
#
#     @abc.abstractmethod
#     def getDBvalue(self):
#         """
#         Abstract formwarding of base class method. Needs to be overwritten by user, in order to
#         define a proper adapter method to write custom data to database.
#         :param value: Custom input values
#         :return: None
#         """
#         return
#
#     @property
#     def value(self):
#         """
#         Simple getter to retrieve a set value from an adapter
#         :return: value if set, otherwise None
#         """
#         if not self.hasValue:
#             print("{} WARN: No value set in this class instance!".format(__class__.__name__))
#             return None
#
#         return self._m_value
#
#     @value.setter
#     def value(self, v):
#         """
#         Simple setter to assign new value to converter instance
#         :return: value if set, otherwise None
#         """
#         assert v is not None
#         self._m_value = v
#         self._m_hasValue = True
#
#
#     @property
#     def datatype(self):
#         """
#         Simple getter to get the adapter value's datatype
#         :return: datatype of value
#         """
#         return self._m_datatype
#
#     @datatype.setter
#     def datatype(self, dt):
#         """
#         Simple setter to set the adapter's datatype after creation
#         :param dt: datatype of value
#         :return: None
#         """
#         assert dt is not None
#         self._m_datatype = dt
#         self._m_hasDatatype = True
#
#     @property
#     def hasDatatype(self):
#         """
#         Simple checker, to check if adapter has datatype set
#         :return: True if set, False otherwise
#         """
#         return self._m_hasDatatype


# class DefaultDBInputAdapter(AbstractDBInputAdapter):
#     """
#     Baseline implementation for default sqlite datatypes.
#     Serves as implementation example as well.
#     """
#     __metaclass__ = abc.ABCMeta
#
#     def __init__(self, column):
#         """
#         Ctor of default input adapter leaf class
#         :param column: Referenced database column
#         """
#         super().__init__(column=column)
#
#     def __conform__(self, protocol):
#         """
#         Implements the class protocol invocation of sqlite's PrepareProtocol, to convert
#         a given value to a sqlite translatable format. In this case it is valid for
#         all default sqlite datatypes, to provide a baseline adapter dealing with reagular
#         values for sqlite datatypes:
#         int   -> INTEGER
#         str   -> TEXT
#         float -> REAL
#         bytes -> BLOB
#         :param protocol: passed by sqlite
#         :return: self.value as either int, str, float or bytes object
#         """
#         if not self.hasValue:
#             print("{} - ERROR no value set, can't invoke adapter to convert it to SQL format".format(__class__.__name__))
#             return
#
#         if protocol is sqlite3.PrepareProtocol:
#             v_t = type(self.value)
#
#             if (v_t is str) or (v_t is int) or (v_t is float) or (v_t is bytes):
#                 return  self.value
#             else:
#                 raise ValueError("{} - ERROR value of adapter is not one of SQLite's default dattypes".format(__class__.__name__))
#
#     def setDBvalue(self, value):
#         self.value = value
#
#
# class DefaultDBOutputAdapter(AbstractDBOutputAdapter):
#     """
#     Baseline implementation for default sqlite datatypes.
#     Serves as implementation example as well.
#     """
#
#     __metaclass__ = abc.ABCMeta
#
#     def __init__(self, column, datatype):
#         """
#         Ctor of default output adapter leaf class
#         :param column: Referenced database column
#         """
#
#         super().__init__(column=column, datatype=datatype)
#
#     def __convert__(self, sql_value):
#         """
#         Converts an input sql_value into the represeantion, specified by the adapter
#         :param sql_value: values as returned from sql query.
#         :return: None
#         """
#         if not self.hasDatatype:
#             print("{} ERROR -- Adapter has not datatype set".format(self.__class__.__name__))
#             assert self.hasDatatype
#
#         ret = None
#
#         if self.datatype == "TEXT":
#             ret = str(sql_value)
#         elif self.datatype == "INTEGER":
#             ret = int(sql_value)
#         elif self.datatype == "REAL":
#             ret = float(sql_value)
#         elif self.datatype == "BLOB":
#             ret = bytes(sql_value)
#         else:
#             print("Datatype does not fit to stored datatypes")
#             pass
#
#         if ret is None:
#             print("{} ERROR -- Adapter's set dataype is not usable for default sql datatypes".format(self.__class__.__name__))
#             assert ret is not None
#
#         self.value = ret
#
#     def getDBvalue(self):
#         """
#         Returns retrieved sql value as user defined datatype
#         :return: value of datatype self.datatype
#         """
#         return self.value