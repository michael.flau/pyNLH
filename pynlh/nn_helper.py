#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Feb 18 21:11:54 2018

@author: mflau
"""

import numpy as np
from .time_helper import genTS

# import tensorflow as tf

# """
# Function to call for a LSTM net definition
# """
#
#
# def defineLSTMNet(n_layers=1, n_neurons=1, activation=None, dropout={'use':False,'rate':0.5}, peepholes = False):
#     layers = []
#     for lidx in range(n_layers):
#         tmp_layer = tf.contrib.rnn.LSTMCell(num_units=n_neurons,
#                                             activation=activation,
#                                             state_is_tuple=False,
#                                             use_peepholes=peepholes)
#
#         layers.append(tmp_layer)
#
#     if dropout['use']:
#         layers = [tf.contrib.rnn.DropoutWrapper(layer, input_keep_prob=dropout['rate'])
#                   for layer in layers]
#
#     return tf.contrib.rnn.MultiRNNCell(layers, state_is_tuple=False)
#
# """
# Function to call for a LSTM net generation
# """
#
#
# def generateLSTMNet(input_x, net_def, dtype=tf.float32):
#
#     assert input_x is not None
#     assert net_def is not None
#     assert dtype is not None
#
#     outputs, states = tf.nn.dynamic_rnn(net_def,
#                                         input_x,
#                                         dtype=dtype)
#
#     return {'hidden_states':outputs, 'output_states':states}
#
# """
# Function to generate a fully connected layer
# basically maps number of input nodes, to number of
# output nodes by applying tf.matmul to each node pair
#
#     input --  output layer of prior network
#     output -- number of output nodes to generate
#     activation -- activation function for each node in layer
# """
#
#
# def generateDenseLayer(input, output, activation=None):
#     assert input is not None
#     assert output is not None
#
#     logits = tf.layers.dense(input,
#                              output,
#                              activation=activation,
#                              name="fully_connected_output_layer")
#     return logits

def computeDecayingLearningRateCurve(numEpochs, lrmax, lrmin):
    """
    Computes the decay of a learning rate
        :param
        :param numEpochs: number of training dataset iterations
        :param lrmax: maximum learning rate
        :param lrmin: minimum learning rate
        :returns
        numpy vector with a learning rate value for each epoch
    """

    assert lrmin > 0.0 and lrmin < 1.0
    assert lrmax > 0.0 and lrmax < 1.0
    assert lrmax > lrmin
    assert numEpochs > 1

    from math import exp

    curve       = np.zeros(1+numEpochs,dtype=np.float64)
    curve[0]    = lrmax
    numEpochs   = np.float64(numEpochs)
    lrmin       = np.float32(lrmin)
    lrmax       = np.float32(lrmax)

    for epoch in range(1,np.int32(numEpochs)):
        curve[epoch] = lrmin + (lrmax-lrmin) * exp(-np.float32(epoch) / numEpochs)

    curve[-1] = lrmin

    return curve


def generateUniqueNNFilenamePart(neurons,
                                 layers,
                                 learning_rate,
                                 steps_t,
                                 input_sz,
                                 output_sz,
                                 epochs,
                                 act=None,
                                 project=None,
                                 net_type=None,
                                 dropout={'use':False,'rate':0.5},
                                 peepholes=False,
                                 momentum={'use':False,'rate':0.99},
                                 comment=None):
    """
    Convenience wrapper to generate unqiue model names for a created neural net model
    :param neurons: Number of neurons
    :param layers: Number of layers
    :param learning_rate: Initial learning rate
    :param steps_t: number of steps
    :param input_sz: input size
    :param output_sz: output size
    :param epochs: Number of epochs
    :param act: Activation type
    :param project: Project name
    :param net_type: Neural Net type
    :param dropout: Uses dropout, bool
    :param peepholes: uses Peepholes, bool
    :param momentum: Uses momentum, bool
    :param comment: Custom comment
    :return: Model name as string object
    """

    out = "{}-".format(genTS())

    if project is None:
        project = "unknown"

    out = "{}project_{}-".format( out, project )

    if net_type is None:
        net_type = "unknown"

    out = "{}net_type_{}-".format(out, net_type)

    if neurons < 0:
        neurons = 0

    out = "{}neurons_{}-".format(out, neurons)

    if layers < 0:
        layers = 0

    out = "{}layers_{}-".format(out, layers)

    if act is None:
        act="unknown"

    out = "{}activation_{}-".format(out, act)
    out = "{}lr_{}-".format(out, learning_rate)
    out = "{}steps_{}-".format(out, steps_t)
    out = "{}input_{}-".format(out, input_sz)
    out = "{}output_{}-".format(out, output_sz)
    out = "{}dropout_{}".format(out, 0 if not dropout['use'] else 1)
    out = "{}_rate_{}".format(out, dropout['rate']) if dropout['use'] else out
    out = "{}-peepholes_{}-".format(out, 0 if not peepholes else 1)
    out = "{}momentum_{}".format(out, 0 if not dropout['use'] else 1)
    out = "{}_rate_{}-".format(out, momentum['rate']) if momentum['use'] else out
    out = "{}_{}".format(out, comment) if comment is not None else out

    return out