# -*- coding: UTF-8 -*-

__author__      = "Michael Flau"
__copyright__   = "Copyright 2019, Michael Flau"
__version__     = "0.1alpha"
__maintainer__  = __author__
__email__       = "michael@flau.net"
__status__      = "Development"
__credits__     = []
__license__     = ""

from .db_helper import *


class AbstractDBAdapter(object):
    """
    Pure virtual base class of all adapters.
    """
    __metaclass__ = abc.ABCMeta

    def __init__(self,column):
        """
        Ctor of abstract base class
        :param column: name of column to be referenced
        """
        assert type(column) is str
        self._m_columnid = column
        self._m_query    = None
        self._m_hasQuery = False
        self._m_value    = None
        self._m_hasValue = False

    @abc.abstractmethod
    def __conform__(self, protocol):
        """
        Converter function which any adapter must implement
        :return: SQL or python representation
        """
        return

    @property
    def targetColumn(self):
        """
        Returns the registered column name as string.
        :return: name of the referenced column as string
        """
        return self._m_columnid

    @property
    def hasValue(self):
        """
        Simple getter to check if adapter has a value set to write/read
        :return:  None
        """
        return self._m_hasValue

    @property
    def hasQuery(self):
        """
        Simple getter to check if adapter has a value set to write/read
        :return:
        """
        return self._m_hasQuery

    @property
    def value(self):
        """
        Simple getter to retrieve a set value from an adapter
        :return: value if set, otherwise None
        """
        if not self.hasValue:
            print("{} WARN: No value set in this class instance!".format(__class__.__name__))
            return None

        return self._m_value

    @property
    def query(self):
        """
        Simple getter to retrieve as set query string from an adapter
        :return: query as str, if query not set returns None
        """
        if not self.hasQuery:
            print("{} WARN: No query set in this class instance!".format(__class__.__name__))
            return None

        return self._m_query

    @query.setter
    def query(self, q):
        """
        Setter method for defining a query string for given adapter
        :param q: SQL query as string
        :return: None
        """
        assert type(q) is str
        self._m_query = q


class AbstractDBInputAdapter(AbstractDBAdapter):
    """
    Base class for all custom adapter classes. This base class dictates which
    methods need to be reimplemented in a adapter leaf class in order to function
    properly within the DBWriter context.
    """
    __metaclass__ = abc.ABCMeta

    def __init__(self, column):
        """
        Ctor of input adapter base class, calls ctor of base class.
        :param column: Name of reference database column.
        """
        super().__init__(column)

    @abc.abstractmethod
    def __conform__(self, protocol):
        """
        Converter function which any adapter must implement. This function basically gives the
        DBWriter instance a recipe for how to convert data from python to SQL.
        :return: SQL or python representation
        """
        return

    @abc.abstractmethod
    def setDBvalue(self, value):
        """
        Abstract formwarding of base class method. Needs to be overwritten by user, in order to
        define a proper adapter method to write custom data to database.
        :param value: Custom input values
        :return: None
        """
        return

    @property
    def value(self):
        """
        Simple getter to retrieve a set value from an adapter
        :return: value if set, otherwise None
        """
        if not self.hasValue:
            print("{} WARN: No value set in this class instance!".format(__class__.__name__))
            return None

        return self._m_value

    @value.setter
    def value(self, v):
        """
        Simple setter to assign a new value to the adapter's value field for later
        database insertion.
        :param v:
        :return:
        """
        assert type(v) is not None
        self._m_value = v
        self._m_hasValue = True


class AbstractDBOutputAdapter(AbstractDBAdapter):
    """
    Base class implementation of custom converter objects.
    Any converter which is supposed to be registered must be derived
    from this base class, in order to fulfill the constraints defined here.
    """
    __metaclass__ = abc.ABCMeta

    def __init__(self, column, datatype = None):
        """
        Ctor of input adapter base class, calls ctor of base class.
        :param column: Name of reference database column.
        """
        super().__init__(column)
        self._m_datatype = datatype
        self._m_hasDatatype = False if datatype is None else True

    @abc.abstractmethod
    def __convert__(self, sql_value):
        """
        Converter function which any output adapter must implement
        :return: SQL to python representation
        """
        return

    @abc.abstractmethod
    def getDBvalue(self):
        """
        Abstract formwarding of base class method. Needs to be overwritten by user, in order to
        define a proper adapter method to write custom data to database.
        :param value: Custom input values
        :return: None
        """
        return

    @property
    def value(self):
        """
        Simple getter to retrieve a set value from an adapter
        :return: value if set, otherwise None
        """
        if not self.hasValue:
            print("{} WARN: No value set in this class instance!".format(__class__.__name__))
            return None

        return self._m_value

    @value.setter
    def value(self, v):
        """
        Simple setter to assign new value to converter instance
        :return: value if set, otherwise None
        """
        assert v is not None
        self._m_value = v
        self._m_hasValue = True


    @property
    def datatype(self):
        """
        Simple getter to get the adapter value's datatype
        :return: datatype of value
        """
        return self._m_datatype

    @datatype.setter
    def datatype(self, dt):
        """
        Simple setter to set the adapter's datatype after creation
        :param dt: datatype of value
        :return: None
        """
        assert dt is not None
        self._m_datatype = dt
        self._m_hasDatatype = True

    @property
    def hasDatatype(self):
        """
        Simple checker, to check if adapter has datatype set
        :return: True if set, False otherwise
        """
        return self._m_hasDatatype


class DefaultDBInputAdapter(AbstractDBInputAdapter):
    """
    Baseline implementation for default sqlite datatypes.
    Serves as implementation example as well.
    """
    __metaclass__ = abc.ABCMeta

    def __init__(self, column):
        """
        Ctor of default input adapter leaf class
        :param column: Referenced database column
        """
        super().__init__(column=column)

    def __conform__(self, protocol):
        """
        Implements the class protocol invocation of sqlite's PrepareProtocol, to convert
        a given value to a sqlite translatable format. In this case it is valid for
        all default sqlite datatypes, to provide a baseline adapter dealing with reagular
        values for sqlite datatypes:
        int   -> INTEGER
        str   -> TEXT
        float -> REAL
        bytes -> BLOB
        :param protocol: passed by sqlite
        :return: self.value as either int, str, float or bytes object
        """
        if not self.hasValue:
            print("{} - ERROR no value set, can't invoke adapter to convert it to SQL format".format(__class__.__name__))
            return

        if protocol is sqlite3.PrepareProtocol:
            v_t = type(self.value)

            if (v_t is str) or (v_t is int) or (v_t is float) or (v_t is bytes):
                return  self.value
            else:
                raise ValueError("{} - ERROR value of adapter is not one of SQLite's default dattypes".format(__class__.__name__))

    def setDBvalue(self, value):
        self.value = value


class DefaultDBOutputAdapter(AbstractDBOutputAdapter):
    """
    Baseline implementation for default sqlite datatypes.
    Serves as implementation example as well.
    """

    __metaclass__ = abc.ABCMeta

    def __init__(self, column, datatype):
        """
        Ctor of default output adapter leaf class
        :param column: Referenced database column
        """

        super().__init__(column=column, datatype=datatype)

    def __convert__(self, sql_value):
        """
        Converts an input sql_value into the represeantion, specified by the adapter
        :param sql_value: values as returned from sql query.
        :return: None
        """
        if not self.hasDatatype:
            print("{} ERROR -- Adapter has not datatype set".format(self.__class__.__name__))
            assert self.hasDatatype

        ret = None

        if self.datatype == "TEXT":
            ret = str(sql_value)
        elif self.datatype == "INTEGER":
            ret = int(sql_value)
        elif self.datatype == "REAL":
            ret = float(sql_value)
        elif self.datatype == "BLOB":
            ret = bytes(sql_value)
        else:
            print("Datatype does not fit to stored datatypes")
            pass

        if ret is None:
            print("{} ERROR -- Adapter's set dataype is not usable for default sql datatypes".format(self.__class__.__name__))
            assert ret is not None

        self.value = ret

    def getDBvalue(self):
        """
        Returns retrieved sql value as user defined datatype
        :return: value of datatype self.datatype
        """
        return self.value


class MaxMinMeanPixelValueInputAdapter(AbstractDBInputAdapter):
    __metaclass__ = abc.ABCMeta

    def __init__(self, column):
        """
        Ctor for all fields which store multiple float values in a list
        :param column:
        """
        super().__init__(column=column)

    def __conform__(self, protocol):
        """

        :param protocol:
        :return:
        """

        if not self.hasValue:
            print("{} Error, no value set!".format(__class__.__name__))
            raise ValueError

        ret = None

        if protocol is sqlite3.PrepareProtocol:
            assert type(self.value) is list
            ret = ";".join(["{}".format(single_value) for single_value in self.value])

        # print("Converting value to {}".format(ret))
        return ret

    def setDBvalue(self, value):
        """

        :param value:
        :return:
        """
        assert type(value) is list
        print("Input value {}".format(value))
        self.value = value

    @property
    def value(self):
        """

        :return:
        """
        return self._m_value

    @value.setter
    def value(self, v):
        """

        :param v:
        :return:
        """
        self._m_value = v
        self._m_hasValue = True


class MaxMinMeanPixelValueOutputConverter(AbstractDBOutputAdapter):
    __metaclass__ = abc.ABCMeta

    def __init__(self, column, datatype):
        """

        :param column:
        :param datatype:
        """
        super().__init__(column=column, datatype=datatype)

    def __convert__(self, sql_value):
        """

        :param sql_value:
        :return:
        """

        assert type(sql_value) is str

        if not self.hasDatatype:
            print("{} Error -- Converter has no datatype set")
            assert self.hasDatatype

        if self.datatype is float:
            self.value = [float(split_value) for split_value in sql_value.split(";")]
        elif self.datatype is int:
            self.value = [int(split_value) for split_value in sql_value.split(";")]
        else:
            raise ValueError("{} ERROR -- Set datatype can't be used to convert data!".format(__class__.__name__))

    def getDBvalue(self):
        """

        :return:
        """
        return self.value

    @property
    def value(self):
        """

        :return:
        """
        return self._m_value

    @value.setter
    def value(self, v):
        """

        :param v:
        :return:
        """
        self._m_value = v