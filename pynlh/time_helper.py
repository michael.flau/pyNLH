#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  7 12:17:32 2018

@author: tashtego
time_helper.py
"""

import time
import datetime

class Timestopper(object):
    def __init__(self):
        """
        Time stopper constructor. Initializes all the variables.
        """
        self._tik = None
        self._tok = None
        self._last_measurement = None
        self._measurement_running = False
        self._has_last_measurement = False
        pass

    def start(self):
        """
        Starts a new measurement. Must not be called twice or more continously.
        :return: None
        """

        if self._measurement_running:
            print("Cannot start time measurement, a measurement is already running. Call stop() first.")
            return

        self._measurement_running = True
        self._tik = time.process_time()
        pass

    def stop(self):
        """
        Stops a time measurement, by taking the second timestep. Also computes the elapsed time
        and stores it in self._last_measurement
        :return: None
        """
        if not self._measurement_running:
            print("Cannot stop time measurement, no measurement running. Call start() first.")
            return

        self._tok = time.process_time()
        self._last_measurement = self._tok - self._tik
        self._has_last_measurement = True
        self._measurement_running = False
        pass

    @property
    def elapsed_time(self):
        """
        Property, which returns the elapsed time in seconds between two measurements.
        :return: Elapsed time as float value. Returns defualt if no last measurement is present.
        """
        if self._has_last_measurement:
            return self._last_measurement
        else:
            print("No measurement taken, return default value")
            return 0.0


def genTS():
    from datetime import datetime
    ts = datetime.now()
    
    if ts.month < 10:
        m = "0{}".format(ts.month)
    else:
        m = "{}".format(ts.month)
        
    if ts.day < 10:
        d = "0{}".format(ts.day)
    else:
        d = "{}".format(ts.day)
        
    if ts.hour < 10:
        H = "0{}".format(ts.hour)
    else:
        H = "{}".format(ts.hour)
        
    if ts.minute < 10:
        M = "0{}".format(ts.minute)
    else:
        M = "{}".format(ts.minute)       

    if ts.second < 10:
        S = "0{}".format(ts.second)
    else:
        S = "{}".format(ts.second)
    
    return "{}{}{}-{}{}{}".format(ts.year,m,d,H,M,S)


def unixTSFromString(datestr, fmt):
    return time.mktime( datetime.datetime.strptime(datestr, fmt).timetuple() )