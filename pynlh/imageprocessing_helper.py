#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 5 13:33:04 2018

@author: Michael Flau on tashtego
"""

import copy
import numpy as np


def dataPreprocessing(rawdata, doMaskFixing = False, doPercentileMaxOut = False, rmMean = False):
    """
    Function wrapper for multiple preprocessing functions which may be enabled by
    setting the respective input value to True.
    :param rawdata: Raw image data to process
    :param doMaskFixing: Enables mask fixing. This Option is useful when encounter files are processed
    which contain masked pixels (0 values).
    :param doPercentileMaxOut: Enables percentile contrast adjustment to image.
    :param rmMean:
    Removes mean from data, leaving effectively only outliers. Useful for Deep Learning data preparation.
    :return: Preprocessed image data
    """

    #Convert input to numpy array    
    tmp = cArr2npArr(rawdata)
    
    #Fix mask pixels
    if doMaskFixing:
        tmp = fixMaskPixels(tmp)

    #percentile to apply
    if doPercentileMaxOut:
        pctl = 1.0    
        tmp = maxOutPercentile(tmp, pctl = pctl)
    
    #Remove mean
    if rmMean:
        tmp = removeMean(tmp)
    
    return tmp


def cArr2npArr(rawdata):
    """
    Converts a C typed array to numpy array. This is not a simple conversion.
    This function creates a deep copy of the data and stores the new data in a
    numpy array, to provide the caller with a an independent copy of the data.
    :param rawdata:
    Raw image data as ctypes array
    :return:
    Raw image data copy as numpy.ndarray
    """
    if not isinstance(rawdata,np.ndarray):
        tmp = copy.deepcopy(np.ctypeslib.as_array(rawdata))
    else:
        tmp = copy.deepcopy(rawdata)

    return tmp


def fixMaskPixels(rawdata):
    """
    Fills all mask pixels with global mean value. This function creates a deep
    copy of the input data.
    :param rawdata:
    Raw image data as numpy.ndarray
    :return:
    Raw image data with all mask pixels set to the image data's mean value
    """
    assert isinstance(rawdata, np.ndarray)
    assert (rawdata.any() != None)

    #Deepcopy data to temporary
    tmp = copy.deepcopy(np.ctypeslib.as_array(rawdata))
    #Fill mask pixels with mean values
    tmp[tmp[:] == 0] = np.mean(tmp[tmp[:] != 0])
    return tmp


def maxOutPercentile(rawdata, pctl):
    """
    Find the lowest/highest X percent of pixels and set them to local minimum, maximum.
    This function creates a deep copy of the data.
    :param rawdata: Image data as numpy.ndarray
    :param pctl: Percentile value, must be a float value between 0.1 and 100.0
    :return:
    Contrast adjusted images data as deep copy of input data.
    """
    assert isinstance(rawdata, np.ndarray)
    assert (rawdata.any() != None)
    assert type(pctl) is float
    assert (pctl >= 0.1) and (pctl <= 100.0)

    tmp = copy.deepcopy(np.ctypeslib.as_array(rawdata))

    if pctl < 50.0:
        pctl_a = pctl
        pctl_b = 100.0 - pctl
    else:
        pctl_b = pctl
        pctl_a = 100.0 - pctl_b

    tmp_tobeblack       = tmp[:] < np.percentile(tmp, pctl_a)
    tmp_tobewhite       = tmp[:] > np.percentile(tmp, pctl_b)
    tmp[tmp_tobeblack]  = np.min(tmp)
    tmp[tmp_tobewhite]  = np.max(tmp)
    return tmp


def getMinMax(unscaledData):
    """
    Returns min and max value of an numpy.ndarray.
    :param unscaledData:
    :return: tuple, containing min and max value of input numpy.ndarray
    """
    assert isinstance(unscaledData, np.ndarray)
    return (np.min(unscaledData),np.max(unscaledData))


def rescaleFrameData(unscaledData, fmin = None, fmax = None):
    """
    Scales input data to values between 0.0 and 1.0.
    :param unscaledData:
    Unscaled input data of any value range.
    :param fmin:
    Minimum range value, defaults to None. If None, the minimum of passed image data is assumed as minimum.
    :param fmax:
    Minimum range value, defaults to None. If None, the maximum of passed image data is assumed as maximum.
    :return:
    Imagedata scaled to range 0.0 - 1.0
    """

    assert isinstance(unscaledData, np.ndarray)
    assert (type(fmin) is int) or (type(fmin) is float) or (type(fmin) is np.float64) or (fmin is None)
    assert (type(fmax) is int) or (type(fmax) is float) or (type(fmax) is np.float64) or (fmax is None)

    if not fmin:
        fmin = np.min(unscaledData)
    if not fmax:
        fmax = np.max(unscaledData)

    # Handle outliers
    unscaledData[unscaledData[:] < fmin] = fmin
    unscaledData[unscaledData[:] > fmax] = fmax

    # Scale to range 0.0 - 1.0
    return (unscaledData[:] - fmin) / (fmax - fmin)


def equalizeFrameData(unequalizedData):
    """
    Performs histogram equalization on input image data.
    :param unequalizedData: image data as numpy.ndarray
    :return: Equalized image data.
    """
    assert isinstance(unequalizedData, np.ndarray)

    # Store shape
    s = unequalizedData.shape
    # Get local reference
    d = unequalizedData
    # Get input data, datatype
    dt = unequalizedData.dtype
    # Flatten data
    if d.ndim > 1: d = d.flatten()

    # Scale data
    d_scaled         = rescaleFrameData(unscaledData=d)
    # Readjust scaled data to input value range
    scaledData       = d_scaled * np.iinfo(d.dtype).max
    # Cast data to input datatype
    castedScaledData = castTo(scaledData, dt)
    # Reshape data to input shape
    return castedScaledData.reshape(s)


def equalizePercentileFrameData(unequalizedData, pctl, subrange=None):
    """
    Performs histogram equalization on input image data by using a percentile value for
    contrast enhancement. It is further possible to draw those percentile from a subregion
    of the image, to further blow up the contrast in specified subrange.
    :param unequalizedData: Input image data of type numpy.ndarray
    :param pctl: Percentile value, must be a float value between 0.1 and 100.0
    :param subrange: Sub range in image which will be used for contrast enhancement as tuple.
    Must be a valid region within the image with tuple shape of (y-low, y-high, x-low, x-high).
    :return: Contrast adjusted image as numpy.ndarray
    """
    assert type(unequalizedData) is np.ndarray
    assert type(pctl) is float
    assert (type(subrange) is tuple) or (subrange is None)
    assert (pctl >= 0.1) and (pctl <= 100.0)

    d  = unequalizedData
    s  = unequalizedData.shape

    if subrange is not None:
        assert len(subrange) == 4
        assert len(s) > 1

        if len(s) == 2:
            h = s[0]
            w = s[1]
        elif len(s) == 3:
            h = s[1]
            w = s[2]
        else:
            h = s[-2]
            h = s[-1]

        assert subrange[0] >= 0
        assert subrange[1] > subrange[0]
        assert subrange[2] >= 0
        assert subrange[3] > subrange[2]

        if len(s) == 2:
            d = d[subrange[0]:subrange[1], subrange[2]:subrange[3]]
        elif len(s) == 3:
            d = d[:,subrange[0]:subrange[1], subrange[2]:subrange[3]]
        else:
            d = d[::, subrange[0]:subrange[1], subrange[2]:subrange[3]]


    dt = unequalizedData.dtype

    # Flatten if higher dimensional
    if d.ndim > 1: d = d.flatten()
    if unequalizedData.ndim > 1: unequalizedData = unequalizedData.flatten()

    if pctl < 50.0:
        pctl_a = pctl
        pctl_b = 100.0 - pctl
    else:
        pctl_b = pctl
        pctl_a = 100.0 - pctl_b

    # Get min percentile
    pctlMin    = np.percentile(d, pctl_a)
    # Get max percentile
    pctlMax    = np.percentile(d, pctl_b)
    # Call rescaling with adapted min max values
    d_scaled = rescaleFrameData(unscaledData=unequalizedData, fmin=pctlMin, fmax=pctlMax)
    # Readjust value range to input value range
    scaledPercetiledData = d_scaled * np.iinfo(d.dtype).max
    # Cast to input dataype
    castedScaledData     = castTo(scaledPercetiledData, dt)
    # Reshape to input shape
    return castedScaledData.reshape(s)


def castTo(frame, target_dt):
    """
    Casts a data frame into target datatype.
    :param frame: image data as numpy.ndarray
    :param target_dt: numpy datatype to which data will be casted
    :return: INput image data, casted to input datatype
    """
    assert isinstance(frame, np.ndarray)
    # assert isinstance(target_dt, np.dtype)

    dt_max = np.iinfo(target_dt).max
    scaledData = rescaleFrameData(unscaledData=frame)
    return scaledData * dt_max


def removeMean(rawdata):
    """
    Removes mean from all data points.
    :param rawdata: input image data as numpy.ndarray
    :return: Deep copy of input image data where mean is removed
    """
    assert isinstance(rawdata, np.ndarray)
    assert (rawdata.any() != None)
    tmp = copy.deepcopy(np.ctypeslib.as_array(rawdata))
    mean = np.mean(rawdata)
    tmp = tmp[:] - mean
    return tmp


def normalizeFeatureVectors(unscaledFeatures, x, y, z):
    """
    Scales all input features using zero mean, unit variance
    Unit variance is applied along temporal axis. Hence this step should be done after
    any CNN operation as this destroys any possible spatial pixel context and scales
    it according to the temporal dimension.
    :param unscaledFeatures:
    :param x: Value of x dimension of input data
    :param y: Value of y dimension of input data
    :param z: Value of z dimension of input data
    :return: Returns a scaled and unified feature map as a copy of the input feature map
    """
    #Check dimensional constraints
    np.testing.assert_array_equal( np.array(unscaledFeatures.shape), np.array([z,x,y]) )
    #Create empty float array of same type
    scaledUnifiedFeatures = np.empty_like(unscaledFeatures, dtype='float64')
    
    #Iterate over y dim
    for idx_y in np.arange(0,y):
        #Iterate over x dim
        for idx_x in np.arange(0,x):
            #Extract unscaled festure vector
            unscaledFV  = unscaledFeatures[:,idx_y,idx_x]
            #Compute scaled feature vector
            scaledUnifiedFV    = (unscaledFV[:] - np.mean(unscaledFV[:])) / np.var(unscaledFV[:])
            #insert into output array
            scaledUnifiedFeatures[:,idx_y,idx_x] = scaledUnifiedFV
            
    return scaledUnifiedFeatures


def linearContrast(raw_image, w=7200, h=576, lc_area=(3500, 3510)):
    """
    Apply linear contrast adjustment to an image given it's width and height
    and the start and end boundary of the contrast reference area. This function is useful
    when the single horizontal lines of the image each have a different mean value.
    :param raw_image: Raw image data as numpy.ndarray
    :param w: width of image
    :param h: height of image
    :param lc_area: Area over which to compute the linear contrast.
    :return: Contrast adjusted image.
    """
    assert(raw_image is not None)
    assert type(h) is int
    assert type(w) is int
    assert type(lc_area) is tuple
    assert(w > 0 and h > 0)
    assert(lc_area[0] < lc_area[1])
    assert (lc_area[0] < w and lc_area[1] < w)

    lc_vec = np.mean(raw_image[:,lc_area[0]:lc_area[1]], axis=1)
    return ((raw_image - lc_vec.reshape(h,1)) + np.min(lc_vec))