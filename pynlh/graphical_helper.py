# -*- coding: utf-8 -*-
"""
Created on Fri Mar  2 18:41:23 2018
graphic_helper.py
Helper functions for graphical presentations
@author: Michael Flau
"""

import numpy as np
from .time_helper import genTS
from skimage.transform import resize
import imageio


def arr2gif(arr, path, resize_factor=1.0):
    """
    Converts a 3D numpy array to a GIF sequence
    :param arr: numpy array of shape [X,Y,Z]
    :param path: path to gif location, including target filename
    :param resize_factor: factor by which image is resized
    :return: None
    """

    with imageio.get_writer(path, mode='I') as writer:
        for fidx in range(arr.shape[0]):
            frame = arr[fidx, :, :]
            if resize_factor != 1.0:

                frame = resize(frame,
                               output_shape=(float(frame.shape[0]) * resize_factor,
                                             float(frame.shape[1]) * resize_factor),
                               anti_aliasing=False)

                # writer.append_data( np.array(frame, dtype=np.uint8) )

            writer.append_data( np.array(frame, dtype=np.uint8) )


def batch2gif(batch, path, resize=1.0, y_labels=None, filenames=None, sample_dims=(30,40,40)):
    """
    Convert a batch of samples to gif animations
    :param batch: batch of image data sequences to export
    :param path: export path, without file name
    :param resize: scale factor
    :param y_labels:
    :param filenames:
    :return:
    """

    assert type(batch) is list
    assert type(path) is str
    assert type(resize) is float
    assert (type(y_labels) is list) or (y_labels is None)
    assert (type(filenames) is list) or (filenames is None)
    assert type(sample_dims) is tuple

    frameVectorLength = sample_dims[1]*sample_dims[2]
    numSamples = len(batch) // frameVectorLength
    label_gif_with_truth_value = False
    use_given_filenames = False

    if y_labels is not None:
        label_gif_with_truth_value = True

    if filenames is not None:
        use_given_filenames = True

    tlabel = ""
    filename = ""
    ts = ""

    for arridx in range(numSamples):
        arr = batch[frameVectorLength * arridx:frameVectorLength * (arridx + 1), :]
        arr = arr.reshape(sample_dims[0], sample_dims[1], sample_dims[2])

        # If labels are given, incorporate labels
        if label_gif_with_truth_value:
            tlabel = "--true" if y_labels[arridx] == 1 else "--false"

        # If filenames are given, incorporate filenames
        if use_given_filenames:
            filename = "-{}".format(filenames[arridx])

        # If neither is given, use nom-timestamp as filename
        if not label_gif_with_truth_value and not use_given_filenames:
            ts = genTS()

        arr2gif(arr, path="{}/{}{}{}.gif".format(path, ts, filename, tlabel), resize=resize)


def computeColorGradient2Colors(color1, color2, steps, dtype=np.uint8):
    """
    Computes a color/intensity gradient between 2 given colors with
    n steps defined in steps parameter.
    :param color1:
    Start color, Intensity (single channel) or RGB color (3 Channel) tuple
    :param color2:
    End color, Intensity (single channel) or RGB color (3 Channel) tuple
    :param steps:
    Number of colors to compute between start and end color as positive integer.
    :return:
    Tuple of n colors containing a color/intensity for each step n.
    """
    assert (isinstance(color1, tuple))
    assert (isinstance(color2, tuple))
    assert (len(color1) in (1, 3))
    assert (len(color2) in (1, 3))
    assert len(color1) == len(color2)
    assert isinstance(steps, int)

    channel = len(color1)
    out     = np.zeros(steps*channel).reshape(channel, steps)

    for channel_idx in np.arange(channel, dtype=np.int32):
        out[channel_idx,:] = np.linspace(color1[channel_idx], color2[channel_idx], num=out.shape[1])

    return out