# -*- coding: utf-8 -*-

"""
Created on Sun Feb 18 21:11:54 2018
@author: Michael Flau
"""

import os
import copy
import numpy as np
import h5py
import pathlib
import abc
import glob
import queue
from collections import namedtuple
from skimage import exposure
from skimage.transform import resize

from .mmap_helper import *
from .imageprocessing_helper import *
from .time_helper import *

"""""""""""""""""""""""
***HELPER STRUCTURES***
"""""""""""""""""""""""

ThreadMessage = namedtuple('ThreadMessage', ['thread_id', 'thread_message'])

"""""""""""""""""""""
***DATASET CLASSES***
"""""""""""""""""""""

class GenericUnlabeledFileDataset(metaclass=abc.ABCMeta):

    def __init__(self, **kwargs):

        self._dummy_initialized = True if (len(kwargs.items()) == 0) else False

        if self._dummy_initialized : return;

        required_entries = [{'id':'number_of_files', 'type':(np.int64, np.int32, int)},
                            {'id':'repeated_access', 'type':(bool)}]

        self.__dict__.update((required_dict_entry['id'], None) for required_dict_entry in required_entries)


        for key, value in kwargs.items():
            print("{}: Next item: {}, next value: {}".format(self.__class__.__name__,
                                                             key,
                                                             value))
            for required_entry in required_entries:
                if key == required_entry['id'] and isinstance(value, required_entry['type']):
                    self.__dict__.update({key: value})

        # self._number_of_files = self.__dict__[required_entries[0]['id']] if \
        #     required_entries[0]['id'] in self.__dict__.keys() else None

        self._number_of_files = self.__dict__['number_of_files']
        if self._number_of_files is None:
            print("No 'number_of_files' argument passed to constructor, can't proceed!")
            raise ArgumentError

        self._repeated_access = self.__dict__['repeated_access']

        if self._repeated_access is None:
            print("No 'repeated_access' argument passed to constructor, can't proceed!")
            raise ArgumentError

        self._file_index = 0
        self._file_handle = None
        self._file_handle_open = False if self._file_handle is None else True


    def __del__(self):
        """
        Base class destructor
        """
        if self._file_handle_open: self._closeFileHandle()


    def _increaseFileIndex(self):
        """
        Internal helper method, to increase the internal file index by one.
        :return: None
        """
        self._file_index += 1

        if (self._file_index >= self._number_of_files) and self._repeated_access:
            print("Fileindex reset...")
            self._file_index = 0
        elif (self._file_index >= self._number_of_files) and not self._repeated_access:
            print("Fileindex reached end...")
            self._file_index = self._number_of_files - 1
        else:
            print("Increased file index to {}".format(self._file_index))
            pass


    def _decreaseFileIndex(self):
        """
        Internal helper method, to decrease the file index by one.
        :return: None
        """
        self._file_index -= 1

        if (self._file_index < 0) and self._repeated_access:
            print("Fileindex reset...")
            self._file_index = self._number_of_files-1
        elif (self._file_index < 0) and not self._repeated_access:
            print("Fileindex reached end...")
            self._file_index = 0
        else:
            print("Decreased file index to {}".format(self._file_index))
            pass


    def _sanitizeFileHandle(self):
        """
        Sanitizer for open file handle
        :return:
        """
        assert self._file_handle is not None


    @abc.abstractmethod
    def _closeFileHandle(self):
        """MUST BE IMPLEMENTED BY SUBCLASS"""
        pass


    @abc.abstractmethod
    def _hasMetaField(self, identifier):
        """MUST BE IMPLEMENTED BY SUBCLASS"""
        pass


    @abc.abstractmethod
    def _getMetaFieldValue(self, identifier, index):
        """MUST BE IMPLEMENTED BY SUBCLASS"""
        pass


    @abc.abstractmethod
    def next(self):
        """MUST BE IMPLEMENTED BY SUBCLASS"""
        pass


    @abc.abstractmethod
    def last(self):
        """MUST BE IMPLEMENTED BY SUBCLASS"""
        pass


    @abc.abstractmethod
    def entryAt(self, index):
        """MUST BE IMPLEMENTED BY CHILD CLASSES"""
        pass


    @property
    def number_of_entries(self):
        """
        Returns the number of single entries stored in dataset instance.
        :return: Number of entries as integer
        """
        return self._number_of_files


    @property
    def file_index(self):
        """
        Returns the current file index of the dataset object.
        :return: File index as integer value.
        """
        return self._file_index

class UnlabeledHDF5FileDataset(GenericUnlabeledFileDataset):

    def __init__(self, **kwargs):

        super().__init__(**kwargs)

        self._dummy_initialized = True if (len(kwargs.items()) == 0) else False
        if self._dummy_initialized : return;

        required_entries = [{'id':'hdf5_file_handle', 'type':(h5py._hl.files.File)}]

        self.__dict__.update((required_dict_entry['id'], None) for required_dict_entry in required_entries)

        for key, value in kwargs.items():
            print("{}: Next item: {}, next value: {}".format(self.__class__.__name__,
                                                             key,
                                                             value))
            for required_entry in required_entries:
                if key == required_entry['id'] and isinstance(value, required_entry['type']):
                    self.__dict__.update({key: value})

        self._file_handle = self.__dict__['hdf5_file_handle']

        if self._file_handle is None:
            print("No 'hdf5_file_handle' argument passed to constructor, can't proceed!")
            raise ArgumentError

        self._sanitizeFileHandle()
        self._file_handle_open = True


    def __del__(self):
        """
        Closes open file handle
        :return:
        """
        if self._file_handle_open: self._closeFileHandle()


    def _closeFileHandle(self):
        """
        Closes the open file handle to the HDF5 file, gets called in base class destructor
        :return: None
        """
        self._sanitizeFileHandle()

        try:
            self._file_handle.close()
        except ImportError as e:
            print("h5py reported an error while closing the data file!")

        self._file_handle_open = False


    def _hasMetaField(self, identifier):
        """
        Simple checker, if data node associated with name identifier exisits within stored hdf5 file handle.
        :param identifier:
        string instance, which is to check for existence in open hdf5 file handle
        :return: Boolean value, True if exists, False if not
        """
        self._sanitizeFileHandle()
        return identifier in self._file_handle


    def _getMetaFieldValue(self, identifier, index=None):
        """
        Returns the content of the node referenced by the input identifier
        at current file index position, regardless of the content itself.
        :param identifier:
        :return:
        """
        if not self._hasMetaField(identifier=identifier): return None

        if isinstance(index, (int, np.int32, np.int64)) and index >= 0 and index < self.number_of_entries:
            idx = index
        else:
            idx = self._file_index

        return self._file_handle[identifier][idx]


    def next(self):
        """
        Returns all node values of the opened hdf5 file handle at position of file index.
        :return: dict object containing all node values at index position
        """

        # Check file handle
        self._sanitizeFileHandle()

        # Increase file index by one
        self._increaseFileIndex()

        ret = {}

        # Extract all values at index postion from nodes
        for node_name in self._file_handle:

            if node_name == 'filecount': continue

            # print('Accessing node "{}" at index {}'.format(node_name, self._file_index))
            node_value = self._getMetaFieldValue(identifier=node_name)

            if node_value.shape == (1,):
                node_value = node_value[0]

            ret[node_name] = node_value

        return ret


    def last(self):
        """
        Returns all node values of the opened hdf5 file handle at position of file index.
        :return: dict object containing all node values at index position
        """

        # Check file handle
        self._sanitizeFileHandle()

        # Increase file index by one
        self._increaseFileIndex()

        ret = {}

        # Extract all values at index postion from nodes
        for node_name in self._file_handle:
            # print('Accessing node "{}" at index {}'.format(node_name, self._file_index))
            ret[node_name] = self._getMetaFieldValue(identifier=node_name)

        return ret


    def entryAt(self, index):
        """
        Access file entry at index position. The passed index must be
        within valid bounds of the number of existing entries.
        :param index:
        Integer value between 0 and self._number_of_files
        :return:
        dict object containing the data stored at index position
        """
        assert isinstance(index, (int, np.int32, np.int64, np.int16))
        assert (self.number_of_entries > index) and (index >= 0)

        # Check file handle
        self._sanitizeFileHandle()

        ret = {}

        # Extract all values at index postion from nodes
        for node_name in self._file_handle:

            if node_name == 'filecount': continue
            node_value = self._getMetaFieldValue(identifier=node_name, index=index)

            if node_value.shape == (1,):
                node_value = node_value[0]

            ret[node_name] = node_value

        return ret


class GenericLabeledDataset(metaclass=abc.ABCMeta):

    class DatasetDummyError(Exception):
        """ Dataset is dummy initialized, it cannot be used as proper class instance! """
        pass

    def __init__(self, **kwargs):
        """
        CTOR, initializes the dataset object.
        :param number_of_classes: Determines how many class types are contained in the
        label vector.
        """

        # Set flag to indicate that class was initalized as dummy and cannot be used in production
        self._dummy_initialized = True if (len(kwargs.items()) == 0) else False
        # Return if dummy, nothing to do here
        if self._dummy_initialized: return

        # Dict entries this class requires
        required_entries = [{'id':'number_of_classes', 'type':(np.int64, int)}]
        # Initalize to default value
        self.__dict__.update((required_dict_entry['id'], None) for required_dict_entry in required_entries)

        # Now iterate over kwargs and find entries which match required dict values
        # Update internal value dict, if found.
        for key, value in kwargs.items():
            print("{}: Next item: {}, next value: {}".format(self.__class__.__name__,
                                                             key,
                                                             value))
            for required_entry in required_entries:
                if key == required_entry['id'] and isinstance(value, required_entry['type']):
                    self.__dict__.update({key: value})

        self._X_train = None
        self._X_test = None
        self._X_validate = None
        self._y_train = None
        self._y_test = None
        self._y_validate = None
        self._valid_dataset_subsets = ("test", "train", "val")

        self._num_classes = self.__dict__[required_entries[0]['id']] \
            if required_entries[0]['id'] in self.__dict__.keys() else None

        # Raise error if channel first flag not correctly set
        if self._num_classes is None:
            print("No '{}':{} argument passed to constructor, can't proceed!".format(required_entries[0]['id'],
                                                                                     required_entries[0]['type']))
            raise ArgumentError


    def setSubset(self, X, y, subset_id):
        """
        Setter function, returning the requested subset.
        :param X: The actual data of the subset to store
        :param y: The
        :param subset_id: ID of subset to check as instance of type str. Valid values are
        'train', 'test' and 'val'
        :return: None
        """

        # Check if dummy
        self._isDummyInitialized()

        if self.hasSubset(subset_id): print("Warning, overwriting already set subset!")

        self._sanitizeLabel(y)
        self._sanitizeSubset(X, y)

        if subset_id == "test":
            self._X_test = X
            self._y_test = y
        elif subset_id == "train":
            self._X_train = X
            self._y_train = y
        elif subset_id == "val":
            self._X_validate = X
            self._y_validate = y
        else:
            print("Invalid subset {}. Only ['test'|'train'|'val'] are valid identifiers!".format(subset_id))


    def getSubset(self, subset_id):
        """
        Getter function, returning the requested subset.
        :param subset_id: ID of subset to check as instance of type str. Valid values are
        'train', 'test' and 'val'
        :return: training, test or validation set data as tuple.
        """

        # Check if dummy
        self._isDummyInitialized()

        if not self.hasSubset(subset_id): return (None, None)

        if subset_id == "test":
            return self.training_subset
        elif subset_id == "train":
            return self.test_subset
        elif subset_id == "val":
            return self.validation_subset
        else:
            print("Invalid subset {}. Only ['test'|'train'|'val'] are valid identifiers!".format(subset_id))
            return False


    def hasSubset(self, subset_id):
        """
        Simple checker function, to determine if a subset is contained by current
        dataset object.
        :param subset_id: ID of subset to check as instance of type str. Valid values are
        'train', 'test' and 'val'
        :return:
        Returns True if both data and label are present for
        requested subset else returns False.
        """

        #Check if dummy
        self._isDummyInitialized()

        assert isinstance(subset_id, str)

        if subset_id == "test":
            return (self._X_test is not None) and (self._y_test is not None)
        elif subset_id == "train":
            return (self._X_train is not None) and (self._y_train is not None)
        elif subset_id == "val":
            return (self._X_validate is not None) and (self._y_validate is not None)
        else:
            print("Invalid subset {}. Only ['test'|'train'|'val'] are valid identifiers!".format(subset_id))
            return False


    @abc.abstractstaticmethod
    def datasetReadInjector(hdf5_meta_data: h5py._hl.group.Group) -> (tuple, bool):
        """ ABSTRACT STATIC METHOD, IMPLEMENT IN LEAF CLASSES"""
        pass


    def _sanitizeLabel(self, y):
        """
        Checks the given label vector for it's content, validating
        that there are no more classtypes present as there are label values.
        :param y: list of integers, representing the labels.
        :return: None
        """

        is_list    = isinstance(y, list)
        is_ndarray = isinstance(y, np.ndarray)

        assert is_list or is_ndarray

        if is_list:
            for val in y:
                assert (isinstance(val, int) or (type(val).__module__ == 'numpy'))
                assert val < self._num_classes and val >= 0
        else:
            # Check that there is only one dimension
            assert y.shape.__len__() == 1
            for val in y:
                assert (isinstance(val, int) or (type(val).__module__ == 'numpy'))
                assert val < self._num_classes and val >= 0


    def _sanitizeSubset(self, X, y):
        """
        Sanitizes a subset. Checks if the label vactor and the data vector have the same length.
        :param X: actual subset data
        :param y: data label
        :return: None
        """
        label_are_list    = isinstance(y, list)
        label_are_ndarray = isinstance(y, np.ndarray)
        data_is_list      = isinstance(X, list)
        data_is_ndarray   = isinstance(X, np.ndarray)

        assert (label_are_list and data_is_list) or (label_are_ndarray and data_is_ndarray)

        if label_are_list:
            assert len(X) == len(y)
        else:
            assert X.shape[0] == y.shape[0]


    def _isDummyInitialized(self):
        if self._dummy_initialized:
            raise self.DatasetDummyError


    @property
    @abc.abstractproperty
    def meta_data(self) -> dict:
        """
        Returns class name as str object in a dict. Laf class may use this
        to return further meta information.
        :return: dictionary consisting of all class's meta information
        """

        # Check if dummy
        self._isDummyInitialized()

        return {"dataset_class_name": self.__class__.__name__,
                "number_of_classes": self.number_of_classes}


    @property
    def training_subset(self):
        """
        Grants access to the training dataset as tuple if (X,y) which is
        data (X) and label (y).
        :return: Training data set as tuple.
        """

        # Check if dummy
        self._isDummyInitialized()

        return (self._X_train, self._y_train)


    @property
    def test_subset(self):
        """
        Grants access to the test dataset as tuple if (X,y) which is
        data (X) and label (y).
        :return: Test data set as tuple.
        """

        # Check if dummy
        self._isDummyInitialized()

        return (self._X_test, self._y_test)


    @property
    def validation_subset(self):
        """
        Grants access to the validation dataset as tuple if (X,y) which is
        data (X) and label (y).
        :return: Validation data set as tuple.
        """

        # Check if dummy
        self._isDummyInitialized()

        return (self._X_validate, self._y_validate)


    @property
    def valid_subset_ids(self):
        """
        Returns all IDs of valid dataset subsets
        as strings in a tuple object.
        :return: subset IDs as tuple object
        """

        # Check if dummy
        self._isDummyInitialized()

        return self._valid_dataset_subsets


    @property
    def number_of_classes(self) -> int:
        """
        Returns the number of classes associaed with the current dataset
        :return: Number of classes as integer value
        """

        # Check if dummy
        self._isDummyInitialized()

        return self._num_classes


class LabeledImageDataset(GenericLabeledDataset):

    def __init__(self, **kwargs):
        """
        CTOR
        :param image_dimensions:
        Tuple containing the image dimensions which every frame stored in the single subsets
        consists of.
        :param number_of_classes: Determines how many class types are contained in the
        label vector.
        """

        # Pass kwargs to base class
        super().__init__(**kwargs)

        # return if the class is dummy initialized
        if self._dummy_initialized: return

        # Required dict entries of this class
        allowed_values = [{'id':'image_dimensions', 'type':(tuple)},
                          {'id':'channels_first', 'type':(bool)}]

        # Default initialize values
        self.__dict__.update((value['id'], None) for value in allowed_values)

        # Now iterate over kwargs and find entries which match required dict values
        # Update internal value dict, if found.
        for key, value in kwargs.items():

            print("{}: Next item: {}, next value: {}".format(self.__class__.__name__,
                                                             key,
                                                             value))

            for allowed_value in allowed_values:

                if key == allowed_value['id'] and isinstance(value, allowed_value['type']):
                    self.__dict__.update({key: value})


        self._channel_first = self.__dict__[allowed_values[1]['id']] if allowed_values[1]['id'] in self.__dict__.keys() else None

        # Raise error if channel first flag not correctly set
        if self._channel_first is None:
            print("No '{}':{} argument passed to constructor, can't proceed!".format(allowed_values[1]['id'],
                                                                                     allowed_values[1]['type']))
            raise ArgumentError

        if self._channel_first:
            dim_indeces = [1, 2, 0]
        else:
            dim_indeces = [0, 1, 2]

        img_dims = self.__dict__[ allowed_values[0]['id'] ] if allowed_values[0]['id'] else (0, 0, 0)

        self._sanitizeDimensionValues(img_dims)
        self._frame_width         = img_dims[ dim_indeces[0] ]
        self._frame_height        = img_dims[ dim_indeces[1] ]
        self._frame_colorchannels = img_dims[ dim_indeces[2] ]


    @staticmethod
    def datasetReadInjector(hdf5_meta_data: h5py._hl.group.Group) -> dict:
        """
        This is a code injector specifically written for the labled image dataset.
        It access the hdf5 file values which are specific to labeledimagedataset hdf5 files.
        :return: returns a dictionary containing
        """
        assert isinstance(hdf5_meta_data, h5py._hl.group.Group)

        required_keys = ["image_dimensions", "channels_first"]
        ret_dict      = {}

        for r_key in required_keys:
            assert r_key in hdf5_meta_data.keys()

        for key in hdf5_meta_data.keys():

            if key == required_keys[0]:
                image_dimensions = tuple([value for value in hdf5_meta_data[ key ]])
                assert len(image_dimensions) == 3
                ret_dict[key] = image_dimensions

            if key == required_keys[1]:
                channels_first = bool(hdf5_meta_data[key][()])
                ret_dict[key]  = channels_first

        return ret_dict


    def _sanitizeDimensionValues(self, image_dimensions):
        """
        Sanitizer function which checks if the input image dimensions
        adhere to certain rules, which are:
        * image dimension container must be a tuple
        * tuple lenght must be 3
        * each contained value must be an integer
        :param image_dimensions:
        Tuple containing the image dimensions which every frame stored in the single subsets
        consists of.
        :return: None
        """
        assert isinstance(image_dimensions, tuple)
        assert (len(image_dimensions) == 3)
        for dim in image_dimensions: assert isinstance(dim, (np.int64, int))


    def _sanitizeImageFrameDimensions(self, X):
        """
        Sanitizer, to check whether the dimensions of any frame in the passed image subset
        adheres to the passed image dimensions.
        :param X: Image data subset
        :return: None
        """
        for frame in X:
            assert isinstance(frame, np.ndarray)

            if self._channel_first:
                assert frame.shape[0] == self._frame_colorchannels
                assert frame.shape[1] == self._frame_width
                assert frame.shape[2] == self._frame_height
            else:
                assert frame.shape[0] == self._frame_width
                assert frame.shape[1] == self._frame_height
                assert frame.shape[2] == self._frame_colorchannels


    def setSubset(self, X, y, subset_id):
        """
        Setter function, returning the requested subset.
        :param X: The actual data of the subset to store
        :param y: The
        :param subset_id: ID of subset to check as instance of type str. Valid values are
        'train', 'test' and 'val'
        :return: None
        """

        # Check if dummy
        self._isDummyInitialized()

        self._sanitizeImageFrameDimensions(X)
        super().setSubset(X, y, subset_id)


    @property
    def meta_data(self) -> dict:
        """
        Returns meta data about the stored dataset data. Fields of dict need to have same
        keys as the parameter names of the constructor. Data must be arranged, as passed to the constructor.
        :return: Dictionary of all meta data as passed to the class it's constructor.
        """

        # Check if dummy
        self._isDummyInitialized()

        if self._channel_first:
            dims = (self._frame_colorchannels, self._frame_width, self._frame_height)
        else:
            dims = (self._frame_width, self._frame_height, self._frame_colorchannels)

        return {"dataset_class_name": self.__class__.__name__,
                "image_dimensions": dims,
                "number_of_classes": self.number_of_classes,
                "channels_first": self._channel_first}


class LabeledVideoDataset(GenericLabeledDataset):

    def __init__(self, **kwargs):
        """

        :param kwargs:
        """

        # Pass kwargs to base class
        super().__init__(**kwargs)

        # return here if class is dummy initialized
        if self._dummy_initialized: return

        # required fields by this class
        required_fields = [{'id': 'video_dimensions', 'type':(tuple)},
                           {'id': 'channels_first', 'type':(bool)}]

        # Default initialize values
        self.__dict__.update((value['id'], None) for value in required_fields)

        for key, value in kwargs.items():

            print("{}: Next item: {}, next value: {}".format(self.__class__.__name__,
                                                             key,
                                                             value))

            for required_field in required_fields:

                if key == required_field['id'] and isinstance(value, required_field['type']):
                    self.__dict__.update({key: value})

        self._channel_first = self.__dict__[required_fields[1]['id']] if required_fields[1]['id'] in self.__dict__.keys() else None

        # Raise error if channel first flag not correctly set
        if self._channel_first is None:
            print("No '{}':{} argument passed to constructor, can't proceed!".format(required_fields[1]['id'],
                                                                                     required_fields[1]['type']))
            raise ArgumentError

        if self._channel_first:
            dim_indeces = [1, 2, 3, 0]
        else:
            dim_indeces = [0, 1, 2, 3]

        img_dims = self.__dict__[ required_fields[0]['id'] ] if required_fields[0]['id'] else (0, 0, 0, 0)

        self._sanitizeDimensionValues(img_dims)
        self._frame_count         = img_dims[ dim_indeces[0] ]
        self._frame_width         = img_dims[ dim_indeces[1] ]
        self._frame_height        = img_dims[ dim_indeces[2] ]
        self._frame_colorchannels = img_dims[ dim_indeces[3] ]


    @staticmethod
    def datasetReadInjector(hdf5_meta_data: h5py._hl.group.Group) -> dict:
        """
        This is a code injector specifically written for the labled image dataset.
        It access the hdf5 file values which are specific to labeledimagedataset hdf5 files.
        :param hdf5_meta_data:
        :return:
        """
        assert isinstance(hdf5_meta_data, h5py._hl.group.Group)

        required_keys = ["video_dimensions", "channels_first"]
        ret_dict      = {}

        for r_key in required_keys:
            assert r_key in hdf5_meta_data.keys()

        for key in hdf5_meta_data.keys():

            if key == required_keys[0]:
                image_dimensions = tuple([value for value in hdf5_meta_data[ key ]])
                assert len(image_dimensions) == 4
                ret_dict[key] = image_dimensions

            if key == required_keys[1]:
                channels_first = bool(hdf5_meta_data[key][()])
                ret_dict[key]  = channels_first

        return ret_dict


    def _sanitizeDimensionValues(self, image_dimensions):
        """
        Sanitizer function which checks if the input image dimensions
        adhere to certain rules, which are:
        * image dimension container must be a tuple
        * tuple lenght must be 3
        * each contained value must be an integer
        :param image_dimensions:
        Tuple containing the image dimensions which every frame stored in the single subsets
        consists of.
        :return: None
        """
        assert isinstance(image_dimensions, tuple)
        assert (len(image_dimensions) == 4)
        for dim in image_dimensions: assert isinstance(dim, (np.int64, int))


    def _sanitizeVideoFrameDimensions(self, X):
        """
        Sanitizer, to check whether the dimensions of any frame in the passed image subset
        adheres to the passed image dimensions.
        :param X: Image data subset
        :return: None
        """
        for frame in X:
            assert isinstance(frame, np.ndarray)

            if self._channel_first:
                assert frame.shape[0] == self._frame_colorchannels
                assert frame.shape[1] == self._frame_count
                assert frame.shape[2] == self._frame_width
                assert frame.shape[3] == self._frame_height
            else:
                assert frame.shape[0] == self._frame_count
                assert frame.shape[1] == self._frame_width
                assert frame.shape[2] == self._frame_height
                assert frame.shape[3] == self._frame_colorchannels


    def setSubset(self, X, y, subset_id):
        """
        Setter function, returning the requested subset.
        :param X: The actual data of the subset to store
        :param y: The
        :param subset_id: ID of subset to check as instance of type str. Valid values are
        'train', 'test' and 'val'
        :return: None
        """

        # Check if dummy
        self._isDummyInitialized()
        # Sanitize data input
        self._sanitizeVideoFrameDimensions(X)
        # Call base class method to store data
        super().setSubset(X, y, subset_id)


    @property
    def meta_data(self) -> dict:
        """
        Returns meta data about the stored dataset data. Fields of dict need to have same
        keys as the parameter names of the constructor. Data must be arranged, as passed to the constructor.
        :return: Dictionary of all meta data as passed to the class it's constructor.
        """

        # Check if dummy
        self._isDummyInitialized()

        if self._channel_first:
            dims = (self._frame_colorchannels, self._frame_count, self._frame_width, self._frame_height)
        else:
            dims = (self._frame_count, self._frame_width, self._frame_height, self._frame_colorchannels)

        return {"dataset_class_name": self.__class__.__name__,
                "video_dimensions": dims,
                "number_of_classes": self.number_of_classes,
                "channels_first": self._channel_first}


"""""""""""""""""""""""""""""
***DATASET WRITER CLASSES****
"""""""""""""""""""""""""""""

class GenericDatasetWriter(metaclass=abc.ABCMeta):

    def __init__(self, path: str, filename: str):
        """
        CTOR of generic loader class
        :param path: Path to output directory as str object
        :param filename: Filename of output dataset file as str
        """
        assert isinstance(path, str)
        assert isinstance(filename, str)

        self._filename = filename
        self._path = path
        self._dataset = None
        self._valid_subset_ids = ("train", "test", "val")


    def _sanitizeDatasetStructure(self, ds: GenericLabeledDataset) -> None:
        """
        Simple sanitizer, which check whether the passed dataset adheres to certain
        structural prerequisites.
        :param ds: Dataset object which must be subclass GenericLabeledDataset
        :return: None
        """
        #Number of fields in a subset tuple
        subset_length = 2
        # sanitize dataset type
        assert issubclass(type(ds), GenericLabeledDataset)
        # Sanitize dict fields
        assert ds.hasSubset("train")
        assert ds.hasSubset("test")
        # Sanitize field values
        assert isinstance(ds.test_subset, tuple)
        assert isinstance(ds.training_subset, tuple)
        # Sanitize field value length
        assert len(ds.test_subset) == subset_length
        assert len(ds.training_subset) == subset_length


    def _getCorrectlyShapedPath(self) -> str:
        """
        Returns the dataset path, validated for trailing '/' at the end.
        :return: str object
        """
        if self._path[-1] != '/':
            return self._path + '/'
        else:
            return self._path


    def writeDataset(self) -> None:
        """
        Public write interface, calls leaf class _write implementations.
        :return: None
        """
        self._write()

    @abc.abstractmethod
    def setDataset(self, dataset: GenericLabeledDataset) -> None:
        """ LEAF CLASS IMPLEMENTATION OF DATASET PASSING ROUTINE """
        pass


    @abc.abstractmethod
    def _write(self) -> None:
        """ LEAF CLASS IMPLEMENTATION OF ACTUAL WRITE ROUTINE """
        pass


class HDF5DatasetWriter(GenericDatasetWriter):

    def __init__(self, path: str, filename: str, subset_str="subset", use_gzip_compression=False, gzip_compression_lvl=4):
        """
        CTOR initialized base class part and some leaf class variables.
        :param path: Path to ouput directory.
        :param filename: Filename of dataset HDF5 file
        :param subset_str: custom user definable string by which the single HDF5 field names will be prepended, defaults to "subset"
        :param use_gzip_compression: Flag, determines if gzip compression will be used.
        :param gzip_compression_lvl: Defines the GZIP compression level, if compression is enabled. Default value is 4.
        Level parameter may be choosen freely between 0 and 9, with 9 being the most agressive compression level.
        """
        super().__init__(path=path, filename=filename)

        assert isinstance(use_gzip_compression, bool)
        assert isinstance(gzip_compression_lvl, int)
        assert (gzip_compression_lvl >= 0 and gzip_compression_lvl <= 9)

        self._gzip_compression_enabled = use_gzip_compression
        self._gzip_compression_lvl = gzip_compression_lvl
        self._subset_user_string = subset_str
        pass


    def setDataset(self, dataset: GenericLabeledDataset) -> None:
        """
        Passes the dataset intended to write to file to the writer object.
        Before the dataset reference is stored, it will be sanitized by an internal
        routine.
        :param dataset: Dataset object, must be subclass of  GenericLabeledDataset
        :return: None
        """
        self._sanitizeDatasetStructure(ds=dataset)
        self._dataset = dataset


    def _validateDatasetFilename(self) -> str:
        """
        Validates the filename for it's ending. Filename must be either of
        the following endings:
        * h5
        * H5
        * hdf5
        * HDF5
        :return: Validated filename string s str object
        """
        v1 = self._filename[-5:] != ".hdf5"
        v2 = self._filename[-5:] != ".HDF5"
        v3 = self._filename[-3:] != ".h5"
        v4 = self._filename[-3:] != ".H5"

        if v1 and v2 and v3 and v4:
            return self._filename + ".hdf5"
        else:
            return self._filename


    def _write(self) -> None:
        """
        Actual write implementation of HDF5 dataset writer class
        :return:
        """
        # Get a validated output path string
        l_dir = self._getCorrectlyShapedPath()

        # Create a path object to the output directory
        path_object = pathlib.Path(l_dir)
        path_object.mkdir(parents=True, exist_ok=True)

        # Get a validated output filename
        l_filename = self._validateDatasetFilename()

        print("{}: Writing dataset to {}/{}".format(self.__class__.__name__,
                                                    path_object.as_posix(),
                                                    l_filename))
        # Create the HDF5 output file
        h5_file = h5py.File(path_object / l_filename, "w")
        h5_dataset_group = h5_file.create_group(name="dataset")

        # Stores subset identifiers in file
        h5_subset_indeces = []

        # Store all data and label fields in subsequent datasets
        # for each dataset an indentifier is generated, which is written to
        # an index field, which will useful when reading from that dataset file.
        for subset_id in self._valid_subset_ids:
            if self._dataset.hasSubset(subset_id):
                for subset_part, subset_part_descriptor in zip(self._dataset.getSubset(subset_id), ["data", "label"]):

                    h5_subset_index = "{}_{}_{}".format(self._subset_user_string,
                                                     subset_id,
                                                     subset_part_descriptor)

                    print("Writing dataset part: {}".format(h5_subset_index))

                    h5_subset_indeces.append(h5_subset_index)

                    print("DTYPE of subset: {}".format(subset_part.dtype))

                    if self._gzip_compression_enabled:
                        h5_dataset_group.create_dataset(name=h5_subset_index,
                                               shape=np.shape(subset_part),
                                               data=subset_part,
                                               dtype=subset_part.dtype,
                                               compression="gzip",
                                               compression_opts=self._gzip_compression_lvl)
                    else:
                        h5_dataset_group.create_dataset(name=h5_subset_index,
                                               shape=np.shape(subset_part),
                                               data=subset_part,
                                               dtype=subset_part.dtype)

        # Finally store an index with all fields written to file
        h5_dataset_group.create_dataset(name="index",
                                        data=np.array(h5_subset_indeces, dtype=object),
                                        dtype=h5py.special_dtype(vlen=str))



        ds_meta    = self._dataset.meta_data
        meta_group = h5_file.create_group(name="meta_information")

        for key in ds_meta.keys():
            print("Storing meta data with key:" + key)
            meta_group[key] = ds_meta[key]
            # meta_group.create_dataset(name=key,
            #                           dtype=type(ds_meta[key]),
            #                           data=ds_meta[key])


class SingleFileDatasetWriter(GenericDatasetWriter):

    def __init__(self):
        raise NotImplemented
        pass


    def writeDataset(self):
        raise NotImplemented
        pass


    def setDataset(self, dataset):
        raise NotImplemented
        pass


    def _write(self):
        raise NotImplemented
        pass


"""""""""""""""""""""""""""""
****DATASET READER CLASSES***
"""""""""""""""""""""""""""""


class GenericDatasetLoader(metaclass=abc.ABCMeta):

    def __init__(self, path, filename):
        """

        :param path:
        :param filename:
        """
        # print("Base class loader got dir: {}".format(path))
        # print("Base class loader got file: {}".format(filename))
        self._filename = filename
        self._path = path
        self._dataset = None
        pass


    def loadDataset(self) -> None:
        self._load()
        pass


    @abc.abstractmethod
    def _load(self) -> None:
        """ ABSTRACT BASE, LEAF CLASSES NEED TO IMPLEMENT THIS CLASS """
        pass


    @property
    def dataset(self):
        """

        :return:
        """
        return self._dataset


class HDF5RawShapedDataLoader(GenericDatasetLoader):
    """

    """
    def __init__(self, path, filename, repeated_access=False):

        # Call base class
        super().__init__(path=path, filename=filename)

        self._num_files = None
        self._current_file_index = None
        self._repeats = repeated_access
        self._shapedRawData = None


    def _load(self) -> None:


        print("{}: Loading {}{}".format(self.__class__.__name__,
                                        self._path,
                                        self._filename))

        fobject = pathlib.Path(self._path + self._filename)
        assert fobject.exists() and fobject.is_file()

        h5_file = h5py.File(fobject, "r")

        number_of_files = h5_file["/filecount"][0]
        self._dataset = UnlabeledHDF5FileDataset(number_of_files=number_of_files,
                                                 repeated_access=self._repeats,
                                                 hdf5_file_handle=h5_file)





class HDF5DatasetLoader(GenericDatasetLoader):

    def __init__(self, path, filename):
        """
        CTOR, calls base class part. Initializes leaf class variables.
        :param path: Path to directory where hdf5 dataset lies as str object
        :param filename: Filename of hdf5 which contains dataset
        """
        super().__init__(path=path, filename=filename)


    def _load(self) -> None:
        """
        Specialized implementation on how to read dataset data from a hdf5 file.
        The type of dataset is not known prior to accessing the file. All information
        on which dataset class to use is extracted from the df5 file and then used to
        create the correct dataset classtype via introspection.
        :return: None
        """
        print("{}: Loading {}/{}".format(self.__class__.__name__, self._path, self._filename))
        path_object = pathlib.Path(self._path)

        # Open the hdf5 file
        assert path_object.exists() and path_object.is_dir()
        h5_file = h5py.File(path_object / self._filename, "r")

        # Extract meta information
        meta_id = '/meta_information'
        assert meta_id in h5_file
        meta_information = h5_file["/meta_information"]

        ds_dict = {}

        # Find common datasets arguments, each dataset has
        for key in meta_information.keys():

            if key == "dataset_class_name":
                dataset_class = meta_information[key][()]

            if key == "number_of_classes":
                number_of_label_classes = meta_information[key][()]
                ds_dict[key] = number_of_label_classes

        # Create a temporary dataset instance of specified class type
        # and use the defined datasetReadInjector static method, to
        # extract class specific start arguments
        ds_specific_information = globals()[dataset_class]().datasetReadInjector(meta_information)

        # Merge common arguments with class specific arguments
        ds_dict = {**ds_dict, **ds_specific_information}

        print("Dynamically creating dataset of type: {}".format(dataset_class))
        print("Arguments to pass are: {}".format(ds_dict))

        dataset = globals()[dataset_class](**ds_dict)

        # Now extract data based on created dataset instance
        index_id = '/dataset/index'
        assert index_id in h5_file
        data_fields = h5_file["/dataset/index"]

        # Create temporary subset storage
        subsets = {}
        for subset_id in dataset.valid_subset_ids:
            subsets[subset_id] = {'data': None, 'label': None}

        # Iterate over dataset fields, and pass single dataset parts into temporary subset storage
        for dfield in data_fields:
            print("{}: Next field in /dataset: {}".format(self.__class__.__name__, dfield))

            id_parts = dfield.split('_')
            assert len(id_parts) == 3
            subset = id_parts[1]
            subset_part = id_parts[2]

            subsets[subset][subset_part] = np.array(h5_file["/dataset/{}".format(dfield)]).astype(np.float64)

        # Pass all valid dataset parts to dataset object
        for subset_key in subsets.keys():
            if (subsets[subset_key]['data'] is not None) and (subsets[subset_key]['label'] is not None):
                dataset.setSubset(X = subsets[subset_key]['data'],
                                  y = subsets[subset_key]['label'],
                                  subset_id = subset_key)
            else:
                print("Dataset file does not contain {} subset skipping...".format(subset_key))

        # Store dataset in member for later access
        self._dataset = dataset


"""""""""""""""""""""""
**DATA HELPER CLASSES**
"""""""""""""""""""""""

class ETAW16_BlowData(object):
    def __init__(self):
        self.train_images  = None
        self.train_labels  = None
        self.test_images   = None
        self.test_labels   = None
        self.dataset_ratio = None

    @property
    def get_train_images(self):
        return self.train_images
    @get_train_images.setter
    def set_train_images(self, images):
        self.train_images = images
    @property
    def get_test_images(self):
        return self.test_images
    @get_test_images.setter
    def set_test_images(self, images):
        self.test_images = images

    @property
    def get_train_labels(self):
        return self.train_labels
    @get_train_labels.setter
    def set_train_labels(self, labels):
        self.train_labels = labels
    @property
    def get_test_labels(self):
        return self.test_labels
    @get_train_labels.setter
    def set_test_labels(self, labels):
        self.test_labels = labels
    def load_data(self):
        return (self.train_images, self.train_labels), (self.test_images, self.test_labels)


class ShapedFeatureSet(object):
    """
    Container for shaped feature data
    """
    def __init__(self, y=None, files=None, X=None):
        self.y = y
        self.files = files
        self.X = np.array([])
        if X is not None:
            self.X = X


class TrainTestData(object):
    """
    Container for test and traing data of a data set
    """
    def __init__(self, train=None, test=None):

        if test is None:
            self.test = ShapedFeatureSet()
        else:
            self.test = ShapedFeatureSet(train['y'], train['files'])

        if train is None:
            self.train = ShapedFeatureSet()
        else:
            self.train = ShapedFeatureSet(test['y'], test['files'])

    def setYTrain(self, y):
        self.train.y = y

    def setYTest(self, y):
        self.test.y = y

    def yTrain(self):
        return self.train.y

    def yTest(self):
        return self.test.y

    def setTrainFiles(self, files):
        self.train.files = files

    def setTestFiles(self, files):
        self.test.files = files

    def filesTrain(self):
        return self.train.files

    def filesTest(self):
        return self.test.files

    def setTrainFeatures(self, Xtrain):
        self.train.X = Xtrain

    def setTestFeatures(self, Xtest):
        self.test.X = Xtest

    def getTrainFeatures(self):
        return self.train.X

    def getTestFeatures(self):
        return self.test.X


def loadRandomBatch(data, n_outputs, n_batch, reshape_dims, type="test", dbg=False):
    """
    Generates a random subset of samples from a given data set. returns actual samples,
    labels and dataset indeces for verfication.

        :param: data: Actual data set
        :param: n_outputs: Number of output the dataset is supposed to give
        :param: n_batch: Batch size to return
        :param: reshape_dims: Dimension of samples to return them reshaped
        :param: type: type of data to fetch the subset from [test, train]

        :returns:
        Dictionary:
            X: Subset of samples picked randomly
            y: Assiated truth labels of X
            indeces: Indeces of X samples extracted from input data set
    """

    # Input args check
    assert data is not None
    assert n_outputs is not None
    assert n_batch is not None
    assert reshape_dims is not None
    assert len(reshape_dims) ==  3

    h = reshape_dims[0]
    w = reshape_dims[1]
    t = reshape_dims[2]

    assert h is not None
    assert w is not None
    assert t is not None

    # 1D sample size
    sample1D = h*w
    test_indices = None

    # Preinitialize arrays
    X = np.ones(n_batch * t * sample1D).reshape(n_batch, t, sample1D)
    y = np.ones(n_batch * n_outputs).reshape(n_batch, n_outputs)

    ds = None

    if type == "test":
        ds = data.test
    elif type == "train":
        ds = data.train
    else:
        ds = None

    assert ds is not None

    # Generate test indices whithin data set bounds
    r_indices = np.random.randint((len(ds.X) // sample1D), size=(1, n_batch))
    if dbg: print('Extracting test_batch at:\n{}'.format(r_indices))

    # Extract samples and stor into batch
    for data_idx, cnt_idx in zip(r_indices[0], range(len(r_indices[0]))):
        start = data_idx * sample1D
        end = start + sample1D
        tmp_sample = ds.X[start:end, :].reshape(t, sample1D)
        X[cnt_idx, :, :] = tmp_sample
        y[cnt_idx] = ds.y[data_idx]

    return {"X":X, "y":y, "indices":r_indices}



def loadBatch(data, n_outputs, n_batch, reshape_dims, type="test", iter=1, dbg=False):
    """
    Extract a test batch from given data with respect to batch iteration

    :param: data: input data
    :param: n_outputs: Number of output the dataset is supposed to give
    :param: n_batch: batch size
    :param: reshape_dims: Dimension of samples to return them reshaped
    :param: type: type of data to fetch the subset from [test, train]
    :param: iter: batch iteration, used to compute starting offset when extracting
                        adjacent subsets of files repeatedly, e.g. for training a model
    :return:
        Dictionary:
            X: batch data samples of shape (n_batch, reshape_dims[2], reshape_dims[0]*reshape_dims[1])
            y: batch labels
            indices: batch indices in full data set
    """

    # Input args check
    assert data is not None
    assert n_outputs is not None
    assert n_batch is not None
    assert reshape_dims is not None
    assert len(reshape_dims) ==  3
    assert iter is not None

    height = reshape_dims[0]
    width  = reshape_dims[1]
    frames = reshape_dims[2]

    assert height is not None
    assert width is not None
    assert frames is not None

    # 1D sample size
    sample1D = height * width

    ds = None

    if type == "test":
        ds = data.test
    elif type == "train":
        ds = data.train
    elif type == "both":
        # ds = np.concatenate([data.test, data.train], axis=0)
        new_x = np.concatenate([data.test.X, data.train.X],axis=0)
        new_y = np.concatenate([data.test.y, data.train.y], axis=0)
        new_f = np.concatenate([data.test.files, data.train.files], axis=0)
        ds = namedtuple('KeyEvents', ['X', 'y', 'files'])
        ds.X = new_x
        ds.y = new_y
        ds.files = new_f
    else:
        ds = None

    assert ds is not None

    # Preinitialize arrays
    X = np.ones(n_batch * frames * sample1D).reshape(n_batch, frames, sample1D)
    y = np.ones(n_batch * n_outputs).reshape(n_batch, n_outputs)
    indices = np.ones(n_batch, dtype=np.int32)
    offset = iter * sample1D * n_batch
    for batch_idx in range(n_batch):

        start = offset + (batch_idx * sample1D)
        end = start + sample1D

        assert (ds.X.shape[0] >= end)

        tmp_sample = ds.X[start:end, :].reshape(frames, sample1D)

        indices[batch_idx] = start

        X[batch_idx, :, :] = tmp_sample
        y[batch_idx] = ds.y[(iter * n_batch) + batch_idx]

    if dbg: print("Batch indices:\n{}".format(indices))

    return {"X":X, "y":y, "indices":indices}


def loadGraphTestSubset(data, ratio=0.1, seed=None):
    """
    Loads a subset of all training data, to process.
    The graph can be test faster with a smaller training set.

    :param: data: input data set
    :param: ratio: amount of samples extracted from input data set

    :return:
    A data set of same shape as the input data set, but smaller
    defined by passed ratio
    """

    assert (ratio <= 1.0) and (ratio > 0.0)
    assert data is not None

    snpl = 1600
    print('length of train X: {}'.format(len(data.train.y)))
    print('length of test X: {}'.format(len(data.test.y)))

    numTrainSamples = int(int(len(data.train.y)) * ratio)
    numTestSamples  = int(int(len(data.test.y)) * ratio)

    np.random.seed(seed)
    randTrainIndices = np.random.permutation(numTrainSamples)
    np.random.seed(seed)
    randTestIndices  = np.random.permutation(numTestSamples)

    train = ShapedFeatureSet(data.train.y[randTrainIndices],
                             data.train.files[randTrainIndices])

    X_tmp_train = np.ones(30*snpl*numTrainSamples).reshape(snpl*numTrainSamples,30)
    for cnt, idx in zip(range(len(randTrainIndices)),randTrainIndices):
        # destination array
        start_d = cnt*snpl
        end_d = start_d + snpl
        # source array
        start_s = snpl*idx
        end_s = start_s + snpl

        X_tmp_train[start_d:end_d,:] = data.train.X[start_s:end_s,:]

    train.X = X_tmp_train

    test = ShapedFeatureSet(data.test.y[randTestIndices],
                            data.test.files[randTestIndices])

    X_tmp_test = np.ones(30*snpl*numTestSamples).reshape(snpl*numTestSamples,30)
    for cnt, idx in zip(range(len(randTestIndices)),randTestIndices):
        # destination array
        start_d = cnt*snpl
        end_d = start_d + snpl
        # source array
        start_s = snpl*idx
        end_s = start_s + snpl

        X_tmp_test[start_d:end_d,:] = data.train.X[start_s:end_s,:]

    test.X = X_tmp_test

    # Create data object and fill it
    out = TrainTestData()
    # Test data
    out.setTestFeatures(test.X)
    out.setTestFiles(test.files)
    out.setYTest(test.y)
    # Train data
    out.setTrainFeatures(train.X)
    out.setTrainFiles(train.files)
    out.setYTrain(train.y)

    return out


def loadTrainTestData(datapath):
    """
    Load a given pickle training set from given file path

    :param: datapath: path to pickle file
    :return:
        training data as python structure
    """
    print("Loading from " + datapath)
    filename = datapath.split('/')[-1]
    path = '/'.join(datapath.split('/')[:-1]) + '/'
    data_loader = HDF5RawShapedDataLoader(path=path, filename=filename)
    data_loader.loadDataset()
    input_data = data_loader.dataset

    return input_data


def shapeFeatureData2Dict(trueData, falseData):
    """
    Shapes given training or test data filenames into ShapedFeatureSet object
    """
    # Create a label vector with all ones
    y = np.ones(len(trueData) + len(falseData))
    #
    y[len(falseData):] = 0

    # Create y vectors of trues a false
    # yTrue = np.ones(len(trueData))
    # yFalse = np.zeros(len(falseData))
    # Concatenate trues and falses to one vector each
    fList = np.concatenate((trueData, falseData))
    # y = np.concatenate((yFalse, yTrue))
    assert (len(trueData) + len(falseData)) == len(y)
    # Randomize indices and resort entries accoring to randomization
    randomized = np.random.permutation(len(y))
    fList = fList[randomized]
    y = y[randomized]
    # Return ready dictionary
    return {'y': y, 'files': fList}


def validateLabelOrder(set, mode=1):
    """
    Validates the given set of test or training data, whether
    contained filenames align with labeling of samples. This function
    assumes, that correct labels are part of the filename of each sample.
    :param set: Dataset to validate
    :return: None
    """
    filenames = set['files'][:][:, 1]
    labels = set['y']

    assert len(filenames) == len(labels)

    if mode == 1:
        for label, fn in zip(labels,filenames):
            slabel = "true" if label == 1 else "false"
            tmp = fn.split('_')
            tmp = tmp[-1].split('-')[0]
            assert slabel == tmp
    else:
        for label, fn in zip(labels, filenames):
            slabel = "true" if label == 1 else "false"
            tmp = fn.split('/')[-2]
            assert slabel == tmp


def splitTrainTest(flist, ratio):
    """
    Splits a given set of input files into training- and testdata,
    based on input ratio
    :param flist: List of filenames as list object
    :param ratio: Split ratio as float value
    :return: 2 list objects containing training and test dataset
    """
    shuffled_indices = np.random.permutation(len(flist))
    testSetSize = int(len(flist) * ratio)
    testIndices = shuffled_indices[:testSetSize]
    trainIndices = shuffled_indices[testSetSize:]
    return flist[trainIndices], flist[testIndices]


def getInputFileList(path, filetype, batch_index_offset=0):
    """
    Finds all files of a given type in the specified directory.
    :param path: Path to directory to search as string
    :param filetype: Ending of file as string
    :return: List of all found files as list objects, containing absolute filepaths as strins
    """

    import glob
    import pathlib

    # Index counter
    batch_index = batch_index_offset
    local_path  = path
    local_path += '/' if local_path[-1] != '/' else ''

    input_filelist = glob.glob(pathname=(local_path + '*.' + filetype))

    # return list
    out_filelist = [{"batch_index": None,
                     "filename": None,
                     "path": None} for i in range(len(input_filelist))]

    # iterate over dir entries
    for fileidx, file in zip(range(0, len(input_filelist)), input_filelist):

        # handle if filetype matches
        fo = pathlib.Path(file)

        if fo.is_file() and fo.exists():
            # Append to return list
            bi = batch_index - batch_index_offset
            out_filelist[bi]["batch_index"] = batch_index
            out_filelist[bi]["filename"] = file.split('/')[-1]
            out_filelist[bi]["path"] = local_path
            batch_index += 1

    out_filelist = out_filelist[0:batch_index]

    return out_filelist


def dataToFeatureMap(data, m, n, yPos, file_height=576, file_width=101, file_frames=30, dbg=False):
    """
    Extracts feature vectors of size MxN at position yPos
    and restructures it into a 30 by M by N 3D feature map.
    :param data: Input data as ndarray
    :param m: 1st dimension of output feature vector
    :param n: 2nd dimension of output feature vector
    :param yPos: y position of event in event file
    :param file_height: height of input event file, defaults to 576 (ectr-file)
    :param file_width: width of input event file, defaults to 101 (ectr-file)
    :param file_frames: framecount  of input event file, defaults to 30 (ectr-file)
    :return: featuremap representation of input event file
    """

    assert type(data) is np.ndarray
    assert type(m) is int
    assert type(n) is int
    # assert (type(yPos) is int) and (yPos > 0)  and (yPos < file_height)

    if not isinstance(yPos, int): return None
    if yPos < (n//2): return None
    if yPos >= file_height: return None

    assert type(file_height) is int
    assert type(file_width) is int
    assert type(file_frames) is int

    raw_h = file_height
    raw_w = file_width
    raw_t = file_frames

    featureMap = np.array(np.ones((raw_t, m, n)))

    try:
        for index in np.arange(0, raw_t):
            frame = copy.deepcopy(data[index * raw_h * raw_w:(index + 1) * raw_h * raw_w])
            start = int(raw_w * yPos - (raw_w * raw_t))
            end = int(raw_w * yPos + (raw_w * raw_t))
            framechunk = frame[start:end]

            snippetFrame = np.array([])
            for idx3 in np.arange(n):
                offset = int(idx3 * raw_w)
                sidewidth = int(((raw_w - 1) - m) / 2)
                tmp = framechunk[offset + sidewidth: offset + sidewidth + m]
                if snippetFrame.size == 0:
                    snippetFrame = tmp
                else:
                    snippetFrame = np.concatenate([snippetFrame, tmp])

            if dbg: print(snippetFrame.shape)

            snippetFrame = snippetFrame.reshape(m, n)
            featureMap[index] = snippetFrame

    except ValueError as e:
        print("Error while processing file")
        return None

    return featureMap


def loadECTRFile(path, filename, version = 1):

    if version == 1:
        ectr_type = ECTR
    else:
        ectr_type = ECTR2

    ectr_mmap = mmapAccessor(path + filename, ectr_type)

    if version == 1:
        image_buffer = ectr_mmap.getFieldValue("largebuf")
        py = ectr_mmap.getFieldValue("py")
        yDown = ectr_mmap.getFieldValue("ydown")
    else:
        image_buffer = np.asarray(ectr_mmap.getFieldValue("largeBuffer").snippetData,dtype=np.float64)
        meta = ectr_mmap.getFieldValue("meta")
        # Get height of blow event
        py = meta.posY
        # Get horizon height
        yDown = meta.horizon[meta.posX]

    ectr_mmap.mapclose()

    return (image_buffer, py, yDown)


def loadECTRFileWithMetaData(path, filename, version = 1):

    if version == 1:
        ectr_type = ECTR
    else:
        ectr_type = ECTR2

    ectr_mmap = mmapAccessor(path + filename, ectr_type)

    if version == 1:
        meta = namedtuple(typename='ectr_meta',
                          field_names=['posY','posX','horizon'])
        image_buffer = ectr_mmap.getFieldValue("largebuf")
        meta.posY      = ectr_mmap.getFieldValue("py")
        meta.posX      = ectr_mmap.getFieldValue("px")
        meta.horizon   = ectr_mmap.getFieldValue("ydown")
    else:
        image_buffer = np.asarray(ectr_mmap.getFieldValue("largeBuffer").snippetData,dtype=np.float64)
        meta = ectr_mmap.getFieldValue("meta")
        # Get height of blow event
        # py = meta.posY
        # Get horizon height
        # yDown = meta.horizon[meta.posX]

    ectr_mmap.mapclose()

    return (image_buffer, meta)


def ectrToFeatureMap(ectrFileName, w, h, t, ectr_version=1, rescale_image = False, do_preprocessing = False, dbg=False):
    """
    Convenience wrapper, to bundle bundle most operations defines in this file, to
    extract a feature map of the encounter.
    :param ectrFileName: Absolute path to file
    :param w: width of feature snippet
    :param h: height of feature snippet
    :param t: temporal expansion of snippet
    :param ectr_version:
    Version of encounter file, different file handling is necessary for different versions.
    version 1 --  old version for files which were converted from old svmdb file format
    version 2 -- new version directly generated from tashtego
    :return:
    """

    image_buffer = None
    meta_buffer = namedtuple("meta_info", ['py', 'px', 'ydown'])
    # Open memory map
    if ectr_version == 1:
        mymap = mmapAccessor(ectrFileName, ECTR)
        image_buffer = np.asarray(mymap.getFieldValue("largebuf"), dtype=np.int16)
        # Get height of blow event
        meta_buffer.py = int(mymap.getFieldValue('py'))
        meta_buffer.px = int(mymap.getFieldValue('px'))
        # Get horizon height
        # meta_buffer.ydown = int(mymap.getFieldValue('ydown')[meta_buffer.px])
        meta_buffer.ydown = int(mymap.getFieldValue('ydown'))
    elif ectr_version == 2:
        mymap = mmapAccessor(ectrFileName, ECTR2)
        image_buffer = np.asarray(mymap.getFieldValue("largeBuffer").snippetData, dtype=np.int16)

        meta = mymap.getFieldValue("meta")
        # Get height of blow event
        meta_buffer.py = meta.posY
        meta_buffer.px = meta.posX
        # Get horizon height
        meta_buffer.ydown = meta.horizon[meta.posX]

    # Get access to image data
    # Apply data preprocessing
    if do_preprocessing:
        image_buffer = dataPreprocessing(image_buffer)

    # close mmap
    mymap.mapclose()

    # Skip if event is above horizon
    if meta_buffer.py < meta_buffer.ydown:
        print("Warning: Event Y position is above horizon!")
        return None, meta_buffer

    # Get global min, max values
    if rescale_image:
        gmin, gmax = getMinMax(image_buffer)
        image_buffer = rescaleFrameData(image_buffer, gmin, gmax)

    # Append scaled image to feature map
    feature_map = dataToFeatureMap(image_buffer, w, h, meta_buffer.py, dbg=dbg)

    if feature_map is None:
        return None, None

    feature_map = feature_map.reshape(t, w * h)
    feature_map = feature_map.reshape(w * h, t)

    return feature_map, meta_buffer


def createDataCache(path, entry_list, num_entries):
    """
    Convenience wrapper to create a hdf5 cache_data file.
    :param path: path to filename + filename
    :param dims: size and structue of cache_data file as tuple, e.g. (30,40,40)
    :return:
    Dictionary with fields
        hdf_file_handle: hdf_file_handle to file
        cache_data: access to cache_data structure
    """

    if os.path.isfile(path):
        print("Cache file already exists, deleting old file...")
        os.remove(path)

    hdf_file_handle = h5py.File(path, "a")

    cache_handles = {}

    # Store number of files in hdf file
    filecount_handle = hdf_file_handle.create_dataset("filecount", (1,), dtype=np.int32)
    filecount_handle[0] = num_entries


    for key in entry_list.keys():
        cache_handles[key] = hdf_file_handle.create_dataset(key, (num_entries,) + entry_list[key][0], dtype=entry_list[key][1])

    return (hdf_file_handle, cache_handles)


def filesToRawSet(set_filename, filelist, dims=(40, 40, 30), ectr_version=1, dbg=False, message_queue=None, thread_id=None):
    """
    Logic for opening and extracting data from given set of filenames
    and storing extracted data to a cache file
    :param filelist: List object containing all filenames as string objects
    :param cache: List object which stores featuremaps as continuous datachunks. The object is
    preallocated with zeros and is filled with featuremaps generated from encounter files.
    :param dims: tuple which contains the target dimensions of featuremaps to generate
    :param ectr_version: defines the version of encounter files which is being processed
    :return: returns the cache object, now containing a new featuremap within it
    """
    assert( isinstance(message_queue, queue.Queue) or (message_queue is None) )

    use_message_queue = True if message_queue is not None else False
    num_files    = filelist.shape[0]
    pathstr_len  = len(filelist[0]["path"])
    filename_len = len(filelist[0]["filename"])
    assert num_files > 0

    entry_dims = {}

    # entry_dims["path"] = ((1,), 'S{}'.format(pathstr_len))
    # entry_dims["filename"] = ((1,), 'S{}'.format(filename_len))
    entry_dims["path"] = ((1,), h5py.special_dtype(vlen=str))
    entry_dims["filename"] = ((1,), h5py.special_dtype(vlen=str))
    entry_dims["batch_index"] = ((1,), np.int32)
    entry_dims["px"] = ((1,), np.int32)
    entry_dims["py"] = ((1,), np.int32)
    entry_dims["ydown"] = ((1,), np.int32)
    entry_dims["unannotated_image_dataset"] = ((dims[0] * dims[1], dims[2]),  np.int16)

    # Create chache for output data
    cache_file_handle, data_caches = createDataCache(path=set_filename,
                                                     entry_list=entry_dims,
                                                     num_entries=num_files)
    # template for status update
    printout_template = "{}%: File {}/{} -- Faulty {} -- {}"

    # output write index, independent from process file number
    dataset_index = 0

    # indicates whether read entry is faulty
    faulty_entry = False

    for fileidx, file_object in enumerate(filelist):

        faulty_entry = False

        # Check if last character of path is '/', if not append
        full_file_path = file_object["path"]
        full_file_path += '/' if full_file_path[-1] != '/' else ''
        full_file_path += file_object["filename"]

        # Check if file exists
        ectr_object = pathlib.Path(full_file_path)

        if not ectr_object.exists() or not ectr_object.is_file():
            print("File check of '" + full_file_path + "' failed, check file path!")
            raise ValueError

        # Transform data into right shape
        tmp_featuremap, meta_buffer = ectrToFeatureMap(ectrFileName=full_file_path,
                                                       w=dims[0],
                                                       h=dims[1],
                                                       t=dims[2],
                                                       ectr_version=ectr_version,
                                                       dbg=dbg)

        # create print string
        printout = printout_template.format(int(((fileidx + 1) / num_files) * 100.0),
                                            dataset_index + 1,
                                            num_files,
                                            fileidx - dataset_index,
                                            file_object["filename"])

        if tmp_featuremap is None and meta_buffer is None:
            faulty_entry = True

        # Check if data is valid
        # if tmp_featuremap is None:
        #     tmp_featuremap = np.zeros(dims[0]*dims[1]*dims[2]).reshape(dims[0]*dims[1], dims[2])

        # Store in temporary object for type checking
        tmp_path        = np.string_(file_object["path"]).astype('S')
        if (tmp_path is None) or (tmp_path == ""): faulty_entry = True

        tmp_filename    = np.string_(file_object["filename"]).astype('S')
        if (tmp_filename is None) or (tmp_filename == ""): faulty_entry = True

        tmp_batch_index = file_object["batch_index"]
        if (tmp_batch_index is None) or (tmp_batch_index < 0): faulty_entry = True

        tmp_py          = meta_buffer.py
        if (tmp_py is None) or (tmp_py < 0): faulty_entry = True

        tmp_px          = meta_buffer.px
        if (tmp_px is None) or (tmp_px < 0): faulty_entry = True

        tmp_ydown       = meta_buffer.ydown
        if (tmp_ydown is None) or (tmp_ydown < 0) or (tmp_ydown > tmp_py): faulty_entry = True

        if faulty_entry:
            if use_message_queue:
                tm = ThreadMessage(thread_id=thread_id,
                                   thread_message={'file_index': fileidx,
                                              'processed_index': dataset_index,
                                              'file_volume': num_files,
                                              'status': "skipped"})
                message_queue.put_nowait(tm)
            else:
                printout += " -- SKIPPED"
                print(printout)

            continue

        # Store data in cache file
        data_caches["unannotated_image_dataset"][dataset_index, :] = tmp_featuremap
        # Store path
        data_caches["path"][dataset_index]        = tmp_path
        # Store filename
        data_caches["filename"][dataset_index]    = tmp_filename
        # Store batch_index
        data_caches["batch_index"][dataset_index] = tmp_batch_index
        # Store py
        data_caches["py"][dataset_index]          = tmp_py
        # Store ydown
        data_caches["ydown"][dataset_index]       = tmp_ydown
        # Store px
        data_caches["px"][dataset_index]          = tmp_px

        if use_message_queue:
            tm = ThreadMessage(thread_id=thread_id,
                          thread_message={'file_index': fileidx+1,
                                          'processed_index': dataset_index,
                                          'file_volume': num_files,
                                          'status': "processed"})
            message_queue.put_nowait(tm)
        else:
            # finalize print message
            printout += " -- processed"
            print(printout)

        # Increase output index by one
        dataset_index += 1
        # print("IN LOOP {}".format(thread_id))

    # Send finalizing message
    if use_message_queue:
        tm = ThreadMessage(thread_id=thread_id,
                           thread_message={'file_index': num_files,
                                           'processed_index': num_files,
                                           'file_volume': num_files,
                                           'status': "finished"})
        message_queue.put_nowait(tm)
        # print("Thread is finished!")
        # time.sleep(5)
        # message_queue.join()

    cache_file_handle.close()


def mergeRawHDF5Sets(h5_file_location, mandatory_fields, target_hdf5_file, h5_postfix="hdf5", dbg=False):

    assert(isinstance(h5_file_location, str))

    tmp = pathlib.Path(h5_file_location)
    assert(tmp.exists() and tmp.is_dir())

    assert(isinstance(mandatory_fields, list))
    assert (isinstance(target_hdf5_file, str))

    assert(not pathlib.Path(target_hdf5_file).exists())
    assert(isinstance(h5_postfix, str))
    assert(h5_postfix in ["h5", "hdf5", "H5", "HDF5"])

    fieldID_required = "filecount"
    assert(fieldID_required in mandatory_fields)


    total_entries = 0
    h5_files = glob.glob(h5_file_location+"/*.{}".format(h5_postfix),
                         recursive=False)


    if len(h5_files) == 0:
        print("Could not find any HDF5 files with ending " + h5_postfix)
        return

    sorted_h5_files = []

    for h5_file in h5_files:
        file_name = h5_file.split('/')[-1]
        # print(file_name)
        idx = int(file_name.split('_')[0])
        # print(str(idx))
        sorted_h5_files.append((idx, h5_file))

    sorted_h5_files = sorted(sorted_h5_files, key=lambda entry: entry[0])

    if dbg: print(sorted_h5_files)

    # Check input hdf5 files for correct structure
    for partial_hdf5_set in sorted_h5_files:
        with h5py.File(partial_hdf5_set[1], mode='r') as ds_part:

            for field in mandatory_fields:
                assert (field in ds_part.keys())

            total_entries += ds_part['/'+fieldID_required][0]
    if dbg:
        print("Preparing dataset for {} entries in {} dataset parts".format(total_entries,
                                                                            len(h5_files)))
    # Copy data into one hdf5 file set
    with h5py.File(target_hdf5_file, mode='a') as h5_agg:
        part  = 0
        start = 0
        end   = 0
        for h5_part in sorted_h5_files:

            ds_part = h5py.File(h5_part[1], mode='r')
            end += ds_part["/"+fieldID_required][0]

            if dbg:
                print("Copy data to index region: {} -- {}".format(start, end))

            if part == 0:
                fc_handle = h5_agg.create_dataset("filecount", (1,), dtype=ds_part["/filecount"].dtype)
                fc_handle[0] = total_entries

                for mfield in mandatory_fields:
                    if mfield == fieldID_required: continue
                    field_handle = h5_agg.create_dataset(mfield,
                                                         (total_entries,) + ds_part["/"+mfield].shape[1:],
                                                         ds_part["/"+mfield].dtype)

                    tmp = copy.deepcopy(ds_part["/"+mfield][:])
                    field_handle[start:end,:] = copy.deepcopy(tmp)
                    h5_agg.flush()
            else:
                for mfield in mandatory_fields:
                    if mfield == fieldID_required: continue
                    h5_agg["/"+mfield][start:end,:] = copy.deepcopy(ds_part["/"+mfield][:])
                    h5_agg.flush()

            start = end
            part += 1


def randomizeAndMerge(trues, falses, data_dims):
    """
    Takes 2 ndarray structures of (framecount, width, height, color_channel) shape
    and merges both structures into one. These two structures are regarded as a
    collection of true frames and false frames. Additionally, an ndarray with labels,
    marking the frames as true(1.0) or false(0.0) is generated.
    :param trues:
    ndarray structure of shape (framecount, width, height, color_channel), resembling true image frames
    :param falses:
    ndarray structure of shape (framecount, width, height, color_channel), resembling false image frames
    :param data_dims:
    Tuple which contains all frame dimensions. Accepts only tuple of length 3 of shape (width, height, color_channel)
    :return:
    * Merged and random order relocated data frames in an np.ndarray structure.
    * An np.ndarray structure of shape (num_true+num_false, 1) containing all labels of the returned data structure
    """

    # Sanitize input frames
    assert isinstance(trues, np.ndarray)
    assert isinstance(falses, np.ndarray)
    assert trues.dtype == falses.dtype

    # Sanitize input dim object
    assert isinstance(data_dims, tuple)
    # assert (len(data_dims) == 3) or (len(data_dims) == 4)
    # Check if all dims values are integers
    assert list(map(type, data_dims)) == ([int] * len(data_dims))

    # Check if data structures have same numer of dims as dim tuple
    assert (trues.shape.__len__()  - 1) == len(data_dims)
    assert (falses.shape.__len__() - 1) == len(data_dims)

    # Sanitize input frame dims with input dim tuple
    # for idx in np.arange(0, len(data_dims), dtype=np.int32):

    # print(trues.shape[1:])
    # print(falses.shape[1:])
    # print(data_dims)

    assert trues.shape[1:]  == data_dims
    assert falses.shape[1:] == data_dims

    # Get dimensional values
    # width         = data_dims[0]
    # height        = data_dims[1]
    # color_channel = data_dims[2]

    num_true_frames = trues.shape[0]
    num_frames      = num_true_frames + falses.shape[0]

    # Create a a list of all possible indeces and shuffle it to get random index order
    shuffled_indeces = np.arange(0, num_frames, dtype=np.uint32)
    np.random.shuffle(shuffled_indeces)

    # Create label for data based on shuffled index order
    label = np.zeros(num_frames)
    label[ shuffled_indeces[:num_true_frames] ] = 1.0

    # Create a new storage for all data entities
    # data = np.zeros( num_frames * width * height * color_channel)
    # data = data.reshape( (num_frames, width, height, color_channels) )
    data = np.zeros((num_frames,) + data_dims).astype(trues.dtype)

    # Add true frames to output entity storage
    for true_frame_index, true_frame in zip(shuffled_indeces[:num_true_frames], trues):
        data[true_frame_index, :] = true_frame

    # Add false frames to output entity storage
    for false_frame_index, false_frame in zip(shuffled_indeces[num_true_frames:], falses):
        data[false_frame_index, :] = false_frame

    return (data, label)


def extractSnippetsBasedOnQueryResult(sql_result,
                                      unannotated_image_dataset,
                                      width,
                                      height,
                                      framecount,
                                      data_output_range,
                                      output_channels=1,
                                      rs_factor=1.0,
                                      rescale_on='frame',
                                      rescale_intensities=False,
                                      output_dtype=np.float64):
    """
    Based on the SQL result passed, the image dataset ist accessed and snippets are extracted at the
    locations state in the respective SQL result. Optional snippets can be resized and rescaled to
    match their respective value range.
    :param sql_result:
    :param unannotated_image_dataset:
    :param width:
    :param height:
    :param framecount:
    :param data_output_range:
    :param output_channels:
    :param rs_factor:
    :param rescale_on:
    :param rescale_intensities:
    :return:
    """

    # Validate input arguments
    assert isinstance(unannotated_image_dataset, UnlabeledHDF5FileDataset)
    assert isinstance(sql_result, list)
    assert isinstance(width, int)
    assert isinstance(height, int)
    assert isinstance(framecount, int)
    assert isinstance(output_channels, int)
    assert isinstance(rs_factor, float)
    assert isinstance(rescale_on, str)
    assert isinstance(output_dtype, np.dtype)
    # Validate output range
    assert isinstance(data_output_range, tuple)
    assert len(data_output_range) == 2
    assert data_output_range[0] >= 0
    assert data_output_range[1] > data_output_range[0]
    # Validate options
    assert rescale_on in ['frame', 'snippet']
    assert isinstance(rescale_intensities, bool)

    numpix_per_frame_native = (width * height)
    scaled_w                = int(width * rs_factor)
    scaled_h                = int(height * rs_factor)
    numpix_per_frame_target = scaled_h * scaled_w

    snippet_list = []

    print("Extracting {} snippets based on SQL entries...".format(len(sql_result)))

    for entry in sql_result:

        assert ("data_file_index" in entry)
        assert ("filename" in entry)
        assert ("true_frames" in entry)
        assert ("reviewer_decision" in entry)

        file_idx          = entry["data_file_index"]
        true_frames       = entry["true_frames"]
        reviewer_decision = entry["reviewer_decision"]

        snippet_entry = unannotated_image_dataset.entryAt(file_idx)
        snippet = snippet_entry["unannotated_image_dataset"].reshape(framecount, width, height, 1).astype(np.float64)

        assert entry["filename"] == snippet_entry["filename"]

        # Rescale frame intensities if required
        if rescale_intensities:
            if rescale_on == 'frame':
                target_snippet = np.zeros(snippet.shape, dtype=output_dtype)
                for frame_idx in np.arange(0, framecount):
                    rescaled_frame = exposure.rescale_intensity(snippet[frame_idx,:,:], out_range=data_output_range)
                    target_snippet[frame_idx, :, :] = rescaled_frame.astype(output_dtype)

                snippet = target_snippet

            elif rescale_on == 'snippet':
                snippet = exposure.rescale_intensity(snippet, out_range=data_output_range).astype(output_dtype)

        # Resize image if required
        if rs_factor != 1.0:
            tmp_resized_snippet = np.zeros(scaled_w*scaled_h*framecount).reshape((framecount, scaled_w, scaled_h))

            for frame_idx in np.arange(0, framecount):
                scaled_frame = resize(snippet[frame_idx],
                                      (scaled_w, scaled_h),
                                      order=0,
                                      preserve_range=True)
                tmp_resized_snippet[frame_idx, :, :] = scaled_frame

            snippet = tmp_resized_snippet

        # Multiply channels if output channel > 1
        if output_channels != 1:
            tmp_multichannel_snippet = np.zeros(scaled_w * scaled_h * framecount * output_channels).reshape((framecount, scaled_w, scaled_h, output_channels))

            for frame_idx in np.arange(0, snippet.shape[0]):
                for channel_idx in np.arange(0,output_channels):
                    tmp_multichannel_snippet[frame_idx,:,:,channel_idx] = snippet[frame_idx,:,:,0]

            snippet = tmp_multichannel_snippet

        # Append snippet to snippet list
        snippet_list.append( {"snippet":snippet, "marked_frames":true_frames, "decision":reviewer_decision} )

    # Sanitize snippet list
    assert (len(snippet_list) == len(sql_result))

    return snippet_list


def loadFVI(path, lc_area, type=FVI):
    """
    Convenience load function to retrieve image data from an FVI file,
    if an optional linear contrast area as tuple is passed to the function,
    the linear contrast is already applied.
    :param path: Path to FVi file
    :param lc_area: Area to use for linear contrast computation
    :param type: Type of mmap file to use for FVI loading
    :return: Raw data object of FVI file
    """
    assert(path is not None)
    mapped_fvi = mmapAccessor(path, type)
    imWidth  = mapped_fvi.getFieldValue('width')
    imHeight = mapped_fvi.getFieldValue('height')
    rawData  = mapped_fvi.getFieldValue('data')
    rawData  = dataPreprocessing(rawData, False, False, False)

    mapped_fvi.mapclose()

    if lc_area is not None:
        print("Returning image with contrast adjusted contrast...")
        return linearContrast(rawData, imWidth, imHeight, lc_area)

    print("Returning raw image...")
    return rawData


class BatchFileLoader(object):
    """
    The BatchFileLoader class
    Simple file loader which will load any file which resides in a sub directory of
    a given directory path and which has the stated filetype ending.
    The class reads each file name in that directory and then outputs the next
    entry of the resulting file list by calling next(). If there is no next entry, the default
    behaviour ist, that the last entry in the list is returned. If sinfleReadMode is set to False at
    construction time, the list of entries will be started again at the first entry.
    """
    def __init__(self, dirname, filetype, singleReadMode=True):
        """
        CTOR of BatchFileLoader class.
        Initializes the instance and also reads the files from the filesystem by
        using the passed arguments.
        :param dirname: Path to directory which will be used as root directory.
        :param filetype: Ending of filetype which is searched for.
        :param singleReadMode: Boolean. Defaults to True. File are only traveresed once
        when calling next() and this falg is enabled, otherwise the list will be cycled
        through again from the start.
        """
        assert isinstance(dirname, str)
        assert isinstance(filetype, str)
        assert isinstance(singleReadMode, bool)

        self._dirname    = dirname
        self._filetype   = filetype
        self._singleRead = singleReadMode
        self._filecount  = None
        self._filelist   = []
        self._fileidx    = int(0)
        self._dirRead    = False

        self.readDirectory()


    def readDirectory(self):
        """
        Searches for files of type self._filetype in all subdirectories of
        self.dirname
        :return:
        None
        """
        import glob

        if self._dirRead:
            print("Directory already read...")
            return

        constructed_pattern = self._dirname + "/**/*." + self._filetype
        print("Globbing for pattern: " + constructed_pattern)

        self._filelist = [ file for file in glob.glob(constructed_pattern) ]
        self._filecount = len(self._filelist)
        self._dirRead = True


    def next(self):
        """
        Returns the next file in filename list
        :return:
        """
        if not self.hasNext() or (self._fileidx == self._filecount):
            print("Reached last file!")
            return self._filelist[-1]

        retfile = self._filelist[self._fileidx]

        self._fileidx += 1

        if (self._fileidx == self._filecount) and (not self._singleRead):
            self._fileidx = 0

        return retfile


    def hasNext(self):
        """
        Simple getter, to obtain information about whether the batch loader
        has another file to give.
        :return:
        """
        return True if (not self._singleRead) or \
                       (self._singleRead and (self._fileidx < self._filecount)) \
            else False
