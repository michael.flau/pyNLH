#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Written by Michael Flau
October 2018 -- Initial Version
July 2019 -- Revision
Added comments, fixed some bugs, added support for FLIR M400 FVI files
"""

"""
Created on Tue Oct 27 09:25:47 2018

Helper functions to access memory maps
@author: Michael Flau on tashtego
"""

import mmap
import sys
import os
from ctypes import *
from time import sleep
import numpy


class FVI(Structure):
    _pack_ = 1
    _fields_ = [('counter', c_int),
                ('width', c_int),
                ('height', c_int),
                ('channels', c_int),
                ('time_unix', c_int),
                ('time_ms', c_int),
                ('other', c_int * 94),
                ('data', c_short * 7200 * 576)]


class FVI_M400(Structure):
    _pack_ = 1
    _fields_ = [('counter', c_int),
                ('width', c_int),
                ('height', c_int),
                ('channels', c_int),
                ('time_unix', c_int),
                ('time_ms', c_int),
                ('other', c_int * 94),
                ('data', c_short * 1440 * 1080)]


class ECTR(Structure):
    _pack_ = 1
    _fields_ = [('figure', c_int),
                ('px', c_int),
                ('lock', c_int),
                ('timestamp', c_double),
                ('index', c_int),
                ('size', c_int),
                ('currentbuffer', c_int),
                ('imbuf', c_double * 15870),
                ('name', c_ubyte * 20),
                ('py', c_int),
                ('counter', c_int),
                ('displayindex', c_int),
                ('low', c_double),
                ('high', c_double),
                ('active', c_int),
                ('class', c_int),
                ('prob', c_double),
                ('hposition', c_int * 4),
                ('crosshair', c_double),
                ('crosshair2', c_double),
                ('hbox', c_double),
                ('haxes', c_double),
                ('hcomment', c_double),
                ('hlock', c_double),
                ('h    ', c_double),
                ('hfalse', c_double),
                ('hdiscard', c_double),
                ('himage', c_double),
                ('classf', c_double),
                ('classt', c_double),
                ('probt', c_double),
                ('probf', c_double),
                ('latitude', c_double),
                ('longitude', c_double),
                ('distance', c_double),
                ('heading', c_double),
                ('elevation', c_double),
                ('direction', c_double),
                ('speed', c_double),
                ('course', c_double),
                ('blowlat', c_double),
                ('blowlon', c_double),
                ('localcontrast', c_double),
                ('globalcontrast', c_double),
                ('mitigate', c_double),
                ('largebuf', c_double * 1745280),
                ('auc', c_double),
                ('ydown', c_double),
                ('xmove', c_double),
                ('ymove', c_double),
                ('dmove', c_double),
                ('tmove', c_double),
                ('smove', c_double),
                ('sxmove', c_double),
                ('symove', c_double),
                ('looppos', c_double),
                ('loopdisp', c_double)]


class ECTRdata_s(Structure):
    _pack_ = 1
    _fields_ = [
        ('snippetHeight', c_uint32),
        ('snippetWidth', c_uint32),
        ('snippetFrameCount', c_uint32),
        ('snippetInterFrameDelay', c_uint32),
        ('snippetData', c_short * 15870)
    ]


class ECTRdata_l(Structure):
    _pack_ = 1
    _fields_ = [
        ('snippetHeight', c_uint32),
        ('snippetWidth', c_uint32),
        ('snippetFrameCount', c_uint32),
        ('snippetInterFrameDelay', c_uint32),
        ('snippetData', c_short * 1745280)
    ]

class ECTRMeta(Structure):
    _pack_ = 1
    _fields_ = [
        ('timestamp', c_double),
        ('heading', c_double),
        ('direction', c_double),
        ('course', c_double),
        ('elevation', c_double),
        ('distance', c_double),
        ('local_contrast', c_double),
        ('global_contrast', c_double),
        ('classification', c_double),
        ('latitude', c_double),
        ('longitude', c_double),
        ('user_decision', c_uint32),
        ('mitigation_state', c_uint32),
        ('width_small', c_uint32),
        ('width_large', c_uint32),
        ('height_small', c_uint32),
        ('height_large', c_uint32),
        ('num_frames', c_uint32),
        ('horizon', c_uint32 * 7200),
        ('posX', c_uint32),
        ('posY', c_uint32),
        ('frameindex', c_uint32)
    ]


class ECTR2(Structure):
    _pack_ = 1
    _fields_ = [
        ('magic', c_int32),
        ('version', c_int32),
        ('meta', ECTRMeta),
        ('smallBuffer', ECTRdata_s),
        ('largeBuffer', ECTRdata_l)
    ]


class SceneViewPort(Structure):
    _fields_ = [
        ('x', c_int),
        ('y', c_int),
        ('active', c_bool),
        ('selected', c_bool),
        ('sceneName', c_char * 200)
    ]


class SceneViewPorts(Structure):
    _fields_ = [
        ('scenes', SceneViewPort * 12),
        ('displayScenes', c_bool)
    ]


class IMEMFrame(Structure):
    _fields_ = [
    ('counter', c_double),
    ('done', c_double),
    ('offset', c_double),
    ('high', c_double),
    ('low', c_double),
    ('imagelength', c_double),
    ('ydown', c_double),
    ('turn', c_double),
    ('col1', c_double),
    ('drawBinaryMask', c_double),
    ('applyRunningMean', c_double),
    ('applyGaussian', c_double),
    ('rotation', c_double),
    ('field', c_double),
    ('height', c_double),
    ('nblows', c_double),
    ('blows', c_double * (3 * 100)),
    ('pers_blows', c_double * 22),
    ('last_blow', c_double * 2),
    ('last_click', c_double * 2),
    ('latitude', c_double),
    ('longitude', c_double),
    ('speed', c_double),
    ('course', c_double),
    ('heading', c_double),
    ('time', c_double),
    ('detectvalue', c_double),
    ('detectthresh', c_double),
    ('detectice', c_double),
    ('classify', c_double),
    ('forward', c_double),
    ('guidlines', c_double),
    ('yDVec', c_double * 7200),
    ('dummy', c_double * (100 * 100)),
    ('cmap', c_double * (16384 * 3)),
    ('rmeanboundaries', c_double * 2),
    ('gaussianSettings', c_double * 2),
    ('maskOffset', c_double),
    ('backgroundModel', c_short * (576 * 7200)),
    ('image', c_short * (576 * 7200)),
    ('fullimage', c_short * (576 * 7200)),
    ('viewPorts', SceneViewPorts)
    ]


class IMEM(Structure):
    _fields_ = [
        ('frames', IMEMFrame * 5)
    ]


def openMmap(file, type):
    """

        :param file:
        :param type:
        :return:
        """
    mapObj = {}
    mapObj['filehandle'] = os.open(file, os.O_RDWR | os.O_NONBLOCK)
    mapObj['maphandle'] = mmap.mmap(mapObj['filehandle'], 0, mmap.MAP_SHARED, prot=mmap.PROT_WRITE | mmap.PROT_READ)

    try:
        mapObj['access'] = type.from_buffer(mapObj['maphandle'])
    except ValueError as err:
        print("Error while mapping file to buffer: ", err)
    return mapObj


def closeMmap(mapObj):
    """

    :param mapObj: 
    :return: 
    """
    mapObj['maphandle'].close()
    mapObj['filehandle'].close()


def mapStr2PyStr(mstr):
    """

    :param mstr:
    :return:
    """
    return str(bytes(mstr).decode("utf-8", "ignore"))


class mmapAccessor(object):
    """
    Generic memory map interface class.
    This class can be used to interface a memory map of
    a given class type. All that needs to be passed
    is the path to the memopry map to open and
    the type of the memory map which must be defined
    using ctypes notation.
    """

    def __init__(self, filename, maptype, verbose=False):
        """

        :param filename:
        :param maptype:
        """
        self.m_bHasFilename = True
        self.m_bHasMaptype = True
        self.m_bIsOpen = False
        self.m_Filename = filename
        self.m_MapType = maptype
        self.m_MapObject = {}
        self.m_ExportedPointers = []
        self._verbose = verbose

        if not self.isOpen():
            if self.hasFilename() and self.hasMapType():
                self.open()

    def __del__(self):
        """
        Default destructor of mmap Accessor class
        Attempts to close the memory map on invocation
        at instance's lifetime end.
        """
        if not self.isOpen():
            self.mapclose()

    def isOpen(self):
        """
        Simple getter returning flag value
        which determines if a memory map is open
        :return:
        True on success, False on failure
        """
        return self.m_bIsOpen

    def hasFilename(self):
        """
        Simple getter which checks whether given memory map has
        a filename.
        :return:
        True on success, False on failure
        """
        return self.m_bHasFilename

    def hasMapType(self):
        """
        Simple getter which checks whether given memory map has
        a filetype.
        :return:
        True on success, False on failure
        """
        return self.m_bHasMaptype

    def hasField(self, fieldname):
        """
        Simple getter which checks whether the passed field name exists
        within the loaded memory map definition.
        :param fieldname:
        fieldname value as String object.
        :return:
        True if filedname exists, otherwise False
        """
        for t in self.m_MapObject['access']._fields_:
            if self._verbose: print('searching ' + str(t[0]))
            if str(t[0]) == fieldname:
                return True

        return False

    def open(self):
        """
        Opens target memory map using the values
        which were passed to the object's ctor during
        instanciation.
        :return:
        Nothing, check with isOpen(), if memory was opened
        sucessfully.
        """
        if self.isOpen():
            if self._verbose: print('map already open, cannot open!')
            return

        if not os.path.exists(self.m_Filename):
            if self._verbose: print("file to be mapped does not exists, creating...")
            f = open(self.m_Filename, "wb")
            f.write(sizeof(self.m_MapType()) * b'\0')
            f.close()

        self.m_MapObject['filehandle'] = os.open(self.m_Filename, os.O_RDWR | os.O_NONBLOCK)
        self.m_MapObject['maphandle'] = mmap.mmap(self.m_MapObject['filehandle'], 0, mmap.MAP_SHARED,
                                                  prot=mmap.PROT_WRITE | mmap.PROT_READ)
        self.m_MapObject['access'] = self.m_MapType.from_buffer(self.m_MapObject['maphandle'], 0)
        self.m_bIsOpen = True

    def mapclose(self):
        """
        Closes open memory map object. The instance can't be used anymore
        for data retrieval after calling this method.
        :return:
        None
        """
        if not self.isOpen():
            # print('''mmap object is not open, can't close map''')
            return

        self.m_MapObject['access'] = None

        for ptr in self.m_ExportedPointers:
            ptr = None

        os.close(self.m_MapObject['filehandle'])
        self.m_bIsOpen = False

    def getFieldValue(self, fieldname):
        """
        Returns the value which is referenced with the
        passed fieldname. Check beforehand if the field
        exists, using hasField().
        :param fieldname:
        Fieldname to reference as String object.
        :return:
        Returns the expected field value of the type defined
        by the passed memory map definition. The user needs to
        check the value for correctness. If no field with the
        passed name is found, this method returns 0.
        """
        for t in self.m_MapObject['access']._fields_:
            name = t[0]
            if name == fieldname:
                fieldptr = getattr(self.m_MapObject['access'], name)
                self.m_ExportedPointers.append(fieldptr)
                return fieldptr
        if self._verbose: print('Field not found')
        return 0

    def setFieldValue(self, fieldname, value):
        """
        Writes the passed value to the field with the passed field name.
        Check beforehand if a field exists which is called fieldname with
        hasField(). This method will throw an error if the fieldname which
        is passed was not found within the memory map definition.
        :param fieldname:
        Name of field as String object
        :param value:
        Value to store in field referenced by fieldname
        :return:
        Return True on success, otherwise an ArgumentError gets thrown.
        """
        ret = False

        for t in self.m_MapObject['access']._fields_:
            name = t[0]
            if name == fieldname:
                fieldptr = getattr(self.m_MapObject['access'], name)
                self.m_ExportedPointers.append(fieldptr)

                if type(value) == tuple or type(value) == list or type(value) == dict or type(value) == numpy.ndarray:
                    for idx in range(len(value)):
                        fieldptr[idx] = value[idx]
                else:
                    setattr(self.m_MapObject['access'], fieldname, value)

                ret = True
        if not ret:
            raise ArgumentError('Field with name "' + fieldname + '" not found!')

        return ret

    def getFieldNames(self):
        """
        Request all fieldnames.
        :return:
        Returns a vector with all existing fieldnames as list object.
        """
        if not self.isOpen():
            return []

        fields = []
        for t in self.m_MapObject['access']._fields_:
            fields.append(str(t[0]))

        return fields

    def setFilename(self, filename):
        """
        Use this method to set the path to the memory map after an instance
        has been created. The mmap interface object must not have an open handle to a memory
        map otherwise this method will print a warning and return, doing nothing further.
        :param filename:
        filename as String object
        :return:
        """

        assert type(filename) is str

        if self.isOpen():
            if self._verbose: print("mmap already open, close before changing filename!")
            return
        else:
            self.m_filename = filename

    def setType(self, maptype):
        """
        Use this method to change the memory map definition which is supposed
        to be used when openening a memory mapped file.
        :param maptype:
        Class type of ctypes class definition.
        :return:
        None
        """
        if self.isOpen():
            if self._verbose: print("mmap already open, close before changing filename!")
            return
        else:
            self.m_MapType = maptype
